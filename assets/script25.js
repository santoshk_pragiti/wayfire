if (typeof opcLang == "undefined") {
  var opcLang = {
    shippingMethod: "",
    shippingNotice: "",
    payment: "",
    paymentNotice: "",
    upsellTitle: "",
    upsellAdded: "",
    addToCheckout: "",
  };
}
if (
  typeof opcSettings != "undefined" &&
  typeof opcSettings.checkoutUrl == "undefined"
) {
  opcSettings.checkoutUrl = "singlepagecheckout";
}
(function () {
  var $$,
    k = !1,
    n = !1,
    p = 0,
    q = !1,
    t = 0,
    v = 0,
    x = !1,
    y = !1,
    z = 0,
    A = !1,
    aa =
      '<div class="section section--shipping-method"><div class="section__header">      <h2 class="section__title">' +
      opcLang.shippingMethod +
      '</h2></div><div class="section__content">    <fieldset class="content-box" data-shipping-methods="">    <div class="content-box__row prev_step_wait">' +
      opcLang.shippingNotice +
      "</div>  </fieldset></div></div>",
    E =
      '<div class="section section--payment-method"><div class="section__header">      <h2 class="section__title">' +
      opcLang.payment +
      '</h2></div><div class="section__content">  <fieldset class="content-box">    <div class="content-box__row ship_step_wait">' +
      opcLang.paymentNotice +
      "</div>  </fieldset></div></div>";
  function ba() {
    if ("undefined" === typeof jQuery || 1.7 > parseFloat(jQuery.fn.jquery)) {
      var a = document.createElement("SCRIPT");
      a.src =
        "https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js";
      a.type = "text/javascript";
      document.getElementsByTagName("head")[0].appendChild(a);
      a.onload = function () {
        $$ = jQuery.noConflict(!0);
        F();
        G();
      };
    } else ($$ = jQuery), F(), G();
  }
  function G() {
    ca();
    if (
      window.location.pathname == "/" + opcSettings.checkoutUrl ||
      ("/cart" == window.location.pathname && opcSettings.skipCart)
    )
      ("/cart" == window.location.pathname &&
        (0 == $$("form[action*='cart']").length ||
          $$(".cart-empty:visible").length ||
          $$(".cart--empty-message:visible").length ||
          $$(".cart--empty:visible").length)) ||
        opcLoadCart();
    else {
      var a = function (d) {
        if (!c) {
          H(1);
          c = !0;
          I();
          d = d.closest("form").length
            ? d.closest("form")
            : $$("form[action='/cart/add']");
          var g = d.attr("action");
          $$.ajax({ type: "POST", url: g, data: d.serialize() })
            .done(function () {
              opcLoadCart();
              c = !1;
            })
            .fail(function (h) {
              422 == h.status ? opcLoadCart() : J("cart trigger");
              c = !1;
            });
        }
      };
      if (opcSettings.oneClickBuy) {
        var b = "";
        setInterval(function () {
          b = opcSettings.btnSelector
            ? $$("form button[name='add']," + opcSettings.btnSelector)
            : $$("form button[name='add']");
          if (b.length && !$$(".opcAdd").length) {
            b.off();
            b.parent().attr("style", "position:relative !important");
            var d = b.outerHeight() + "px",
              g = b.outerWidth() + "px",
              h = b.offset();
            b.parent().append(
              '<div class="opcAdd" style="cursor:pointer !important;z-index:9999;opacity:0 !important;height:' +
                d +
                " !important;width:" +
                g +
                ' !important;position:absolute !important;"></div>'
            );
            $$(".opcAdd").offset({ top: h.top, left: h.left });
          }
        }, 200);
      }
      setInterval(function () {
        $$("a[href='/checkout']")
          .off()
          .attr("onclick", "opcLoadCart();return false;");
        $$("button[onclick=\"window.location='/checkout'\"]").attr(
          "onclick",
          "opcLoadCart()"
        );
        var d = $$("button[data-testid='Checkout-button']");
        if (d.length && !$$(".opcQuickAdd").length) {
          d.parent().attr("style", "position:relative !important");
          var g = d.outerHeight() + "px",
            h = d.outerWidth() + "px",
            f = d.offset();
          d.parent().append(
            '<div class="opcQuickAdd" style="cursor:pointer !important;z-index:9999;opacity:0 !important;height:' +
              g +
              " !important;width:" +
              h +
              ' !important;position:absolute !important;"></div>'
          );
          $$(".opcQuickAdd").offset({ top: f.top, left: f.left });
        }
        d = $$("input[name='checkout'],button[name='checkout']").first();
        d.length &&
          !$$(".opcCheckout").length &&
          ((g = d.outerHeight() + "px"),
          (h = d.outerWidth() + "px"),
          (f = d.offset()),
          d
            .parent()
            .append(
              '<div class="opcCheckout" style="cursor:pointer !important;z-index:9999;opacity:0 !important;height:' +
                g +
                " !important;width:" +
                h +
                ' !important;position:absolute !important;"></div>'
            ),
          $$(".opcCheckout").offset({ top: f.top, left: f.left }));
        d = $$("input[name='checkout'],button[name='checkout']").eq(1);
        d.length &&
          !$$(".opcCheckout2").length &&
          ((g = d.outerHeight() + "px"),
          (h = d.outerWidth() + "px"),
          (f = d.offset()),
          d
            .parent()
            .append(
              '<div class="opcCheckout2" style="cursor:pointer !important;z-index:9999;opacity:0 !important;height:' +
                g +
                " !important;width:" +
                h +
                ' !important;position:absolute !important;"></div>'
            ),
          $$(".opcCheckout2").offset({ top: f.top, left: f.left }));
      }, 200);
      setInterval(function () {
        if ($$(".opcCheckout").length) {
          var d = $$("input[name='checkout'],button[name='checkout']")
            .first()
            .offset();
          $$(".opcCheckout").offset({ top: d.top, left: d.left });
        }
        $$(".opcCheckout2").length &&
          ((d = $$("input[name='checkout'],button[name='checkout']")
            .eq(1)
            .offset()),
          $$(".opcCheckout2").offset({ top: d.top, left: d.left }));
      }, 1e3);
      var e =
        ".opcAdd,.opcQuickAdd,.opcCheckout,.opcCheckout2, .cart_buy_button,form input[value='Checkout'],form input[name='checkout'],form input[name='checkout'] *,form button[name='checkout'],form button[name='checkout'] *,button:contains('Checkout'),button:contains('checkout'),button:contains('CHECKOUT')";
      opcSettings.oneClickBuy && (e += ",.ProductForm__AddToCart");
      opcSettings.btnSelector && (e = e + "," + opcSettings.btnSelector);
      $$(e).off();
      e = e.split(",");
      $$.each(e, function (d, g) {
        $$(document).on("click", g, function (h) {
          h.preventDefault();
          a($$(this));
          return !1;
        });
      });
      var c = !1;
    }
  }
  function K() {
    var a = document.createElement("style");
    (document.head || document.getElementsByTagName("head")[0]).appendChild(a);
    a.type = "text/css";
    a.appendChild(
      document.createTextNode(
        "@media (min-width: 750px){.threeCol .field--third {width: 50% !important;}.threeCol #error-for-zip {min-width: 401px;background-color:white;left:20px;top:35px;position:absolute;}.field--required[data-address-field='zip']{position:relative;}}body > :not(#opcUpsellPopup):not(#opcWrapper):not(#opcPopup):not(#opcPreLoader):not(#cartLoader){display:none !important;}.rightCol .section--payment-method{padding-top:20px !important;}.rightCol .section__header{margin-bottom:5px;}div[data-alternative-payments],.section--express-checkout-selection{padding:0px !important;height:0px !important;position:absolute !important;overflow:hidden !important}.step[data-step='payment_method'] form .section:not(.section--reductions){display:block !important;}.threeCol.sidebar::after{width:600%;}.step__footer a{margin-top:10px;}.poptin-popup,.poptin-popup-background,.notifyjs-corner{display:none !important;}.section--billing-address .combo-box{display:none;}.total-line__price span[data-checkout-total-shipping-target]{white-space:normal}.mb_opc.threeCol .total-line__price{width:50%}.section--reductions.hidden-on-desktop{display:none;}.opcErrorBox{box-shadow:0 0 0 1px #e32c2b;border:1px solid #e32c2b}"
      )
    );
  }
  function da() {
    if (window.location.pathname != "/" + opcSettings.checkoutUrl) {
      var a = window.location.href;
      window.history.pushState(
        opcSettings.checkoutUrl,
        "Checkout",
        "/" + opcSettings.checkoutUrl
      );
      $$(window).on("popstate", function () {
        window.location.href = a;
      });
    }
  }
  function ea() {
    function a() {
      q
        ? ($$(".section--contact-information h2").text(
            "1. " + $$(".section--contact-information h2").text()
          ),
          $$(".section--billing-address h2").text(
            "2. " + $$(".section--billing-address h2").text()
          ),
          $$(".section--payment-method h2").text(
            "3. " + $$(".section--payment-method h2").text()
          ),
          setTimeout(function () {
            $$(".section--contact-information h2").length ||
              ($$(".section--billing-address h2").text(
                $$(".section--billing-address h2").text().replace("2. ", "1. ")
              ),
              $$(".section--payment-method h2").text(
                $$(".section--payment-method h2").text().replace("3. ", "2. ")
              ));
          }, 600))
        : ($$(".section--contact-information h2").text(
            "1. " + $$(".section--contact-information h2").text()
          ),
          $$(".section--shipping-address h2").text(
            "2. " + $$(".section--shipping-address h2").text()
          ),
          $$(".section--shipping-method h2").text(
            "3. " + $$(".section--shipping-method h2").text()
          ),
          $$(".section--payment-method h2").text(
            "4. " + $$(".section--payment-method h2").text()
          ),
          setTimeout(function () {
            $$(".section--contact-information h2").length ||
              ($$(".section--shipping-address h2").text(
                $$(".section--shipping-address h2").text().replace("2. ", "1. ")
              ),
              $$(".section--shipping-method h2").text(
                $$(".section--shipping-method h2").text().replace("3. ", "2. ")
              ),
              $$(".section--payment-method h2").text(
                $$(".section--payment-method h2").text().replace("4. ", "3. ")
              ));
          }, 600));
    }
    function b() {
      0 == d &&
        ($$("input[value='credit_card']").length
          ? ($$("input[value='credit_card']").click(), (d = !0))
          : $$(
              ".section--express-checkout-selection input:not([value^='paypal'])"
            ).length &&
            ($$(
              ".section--express-checkout-selection input:not([value^='paypal'])"
            )
              .first()
              .click(),
            (d = !0)));
    }
    var e = 0,
      c = !1,
      d = !1,
      g = setInterval(function () {
        e++;
        0 < $$(".section--express-checkout-selection").length &&
          ($$("input[value='credit_card']").length
            ? ($$("input[value='credit_card']").click(), (d = !0))
            : $$(
                ".section--express-checkout-selection input:not([value^='paypal'])"
              ).length
            ? ($$(
                ".section--express-checkout-selection input:not([value^='paypal'])"
              )
                .first()
                .click(),
              (d = !0))
            : $$(".section--express-checkout-selection input").first().click());
        0 < $$(".step__footer").length &&
          !c &&
          ($$(".section--contact-information2").wrap("<div></div>"),
          $$(".section--contact-information2")
            .removeClass("section--contact-information2")
            .addClass("section--contact-information"),
          $$("div[data-section='customer-information']").hasClass("merged") &&
            0 == $(".section--contact-information").parent("*[class]").length &&
            $$(".section--contact-information").parent().remove(),
          $$(".content").addClass("mb_opc"),
          clearInterval(g),
          (c = !0),
          fa(),
          b(),
          setTimeout(function () {
            b();
          }, 5e3),
          $$("#opcWrapper").first().prevAll("link").clone().appendTo("head"),
          $$("#opcWrapper")
            .first()
            .prevAll()
            .not("div,a,button,span,img,link")
            .appendTo("head"),
          $$("#opcPreLoader").remove(),
          $$(".step__footer").detach().appendTo(".main__content"),
          $$(".breadcrumb,.breadcrumb_mobile").remove(),
          $$(".section__header p").remove(),
          L(),
          ha(),
          ia(),
          ja(),
          ka(),
          $$(
            ".step[data-step='payment_method'] .section:not(.section--reductions)"
          ).hide(),
          la(),
          M(),
          H(3),
          a(),
          ma(),
          na(),
          N());
      }, 20);
  }
  function M() {
    3 != opcSettings.design ||
      q ||
      1300 > $$(window).width() ||
      ($$("body").css("zoom", "0.91"),
      $$(".field:not(.field--email_or_phone,.field--third)").each(function () {
        $$(this).find("#checkout_reduction_code").length ||
          $$(this).find("#checkout_email").length ||
          $$(this).addClass("field--half");
      }),
      setInterval(function () {
        $$(".field:not(.field--email_or_phone,.field--third)").each(
          function () {
            $$(this).hasClass("field--half") ||
              $$(this).find("#checkout_reduction_code").length ||
              $$(this).find("#checkout_email").length ||
              $$(this).addClass("field--half");
          }
        );
        $$(".step").css("min-height", $$(".rightCol").height() + "px");
      }, 500),
      $$(".siderbar").addClass("threeCol"),
      $$(".content").addClass("threeCol"),
      $$(".section--remember-me").remove(),
      $$(".section--shipping-method").addClass("field--half"),
      $$(".section--payment-method").addClass("field--half"),
      $$(".step,.section--shipping-method,.section--payment-method").css(
        "width",
        "381px"
      ),
      $$(".step[data-step='contact_information']").wrap(
        "<div class='leftCol' style='float:left;'></div>"
      ),
      $$(".section--shipping-method").first().appendTo(".leftCol"),
      $$(".section--payment-method").wrap(
        "<div class='rightCol' style='float:left;margin-left:27px;'></div>"
      ),
      $$(".section--billing-address").first().appendTo(".rightCol"),
      $$(".step__footer").first().appendTo(".rightCol"),
      $$(".rightCol").css({
        position: "absolute",
        left: "430px",
        top: "0",
        margin: "0",
        padding: "0",
      }),
      $$(".section--payment-method").css({ margin: "0", padding: "0" }),
      $$(".main__content").css("position", "relative"),
      $$(".main").css("padding", "5px 36px 0 0"),
      $$(".main__header").css("padding-bottom", "0"),
      $$("input[name='checkout[remember_me]']").parents(".field").hide(),
      $$(".section__header").css("margin-bottom", "5px"),
      $$(".section:not(.section .section)").css("padding-top", "20px"),
      $$(".section--contact-information").css("padding-top", "0"),
      $$(".section--contact-information .section__header").css(
        "margin-top",
        "20px"
      ),
      $$(".section--contact-information").css("padding-top", "0"),
      $$(".sidebar").css("width", "288px"),
      $$(".wrap").css("max-width", "1200px"));
  }
  function ha() {
    $$("#notice-complementary").length && J("notice complementary");
    "payment_method" == $$('input[name="step"]').val() && (q = !0);
    $$(".step  .edit_checkout")
      .unbind()
      .submit(function (a) {
        a.preventDefault();
      });
    q || $$(".step__footer").before(aa);
    $$(".step__footer").before(E);
    $$(".section--shipping-address").show();
    $$("#continue_button").removeClass("hidden");
    $$("#payment_button").hide();
    5 < $$(".edit_checkout input:visible").length && !O()
      ? q
        ? P()
        : submitFirstStep()
      : setTimeout(function () {
          5 < $$(".edit_checkout input:visible").length &&
            !O() &&
            (q ? P() : submitFirstStep());
        }, 3e3);
    oa();
    $$("body").on(
      "click",
      ".order-summary__sections form button:not(#continue_button):not(.tag__button)",
      function () {
        Q(!1);
        return !1;
      }
    );
    $$("body").on(
      "click",
      '.step[data-step="payment_method"] button:not(#continue_button):not(.tag__button)',
      function () {
        Q(!0);
        return !1;
      }
    );
    $$("body").on(
      "click",
      ".order-summary__sections form button.tag__button",
      function () {
        R(!1);
        return !1;
      }
    );
    $$("body").on(
      "click",
      '.step[data-step="payment_method"] button.tag__button',
      function () {
        R(!0);
        return !1;
      }
    );
    $$("body").on("click", ".step__footer button", function () {
      $$(this).addClass("btn--loading").attr("aria-busy", "true");
    });
    $$("body").on(
      "click",
      "#continue_button,button.step__footer__continue-btn",
      function () {
        H(11);
        var a,
          b =
            '<span id="errorArrow" style="display:none;font-size: 40px;position:absolute;color: red;">' +
            (S() ? "&#8678;" : "&#8680;") +
            "</span>";
        $$("#opcWrapper").append(b);
        return (a = O())
          ? ($$("#" + a)
              .closest(".field")
              .addClass("field field--required field--error"),
            document.getElementById(a).scrollIntoView(),
            window.scrollBy(0, -100),
            $$(".step__footer button")
              .attr("aria-busy", "false")
              .removeClass("btn--loading"),
            (b = $$("#" + a).offset()),
            (a = $$("#" + a).outerWidth()),
            $$("#errorArrow").show(),
            S()
              ? ($$("#errorArrow").offset({
                  top: b.top + 10,
                  left: b.left + a - 25,
                }),
                window.errorArrow(!0))
              : ($$("#errorArrow").offset({
                  top: b.top + 10,
                  left: b.left - 35,
                }),
                window.errorArrow(!1)),
            alert(opcLang.shippingNotice),
            !1)
          : $$(".ship_step_wait").length && !q
          ? ($$("body,html").animate(
              {
                scrollTop:
                  $$(".section--shipping-method").first().offset().top - 40,
              },
              400
            ),
            $$(
              ".section--shipping-method .section__content .content-box[data-shipping-methods]"
            ).addClass("opcErrorBox"),
            (b = $$(".section--shipping-method .section__content").offset()),
            (a = $$(
              ".section--shipping-method .section__content"
            ).outerWidth()),
            $$("#errorArrow").show(),
            S()
              ? (1 < $$(".section--shipping-method .radio-wrapper").length
                  ? $$("#errorArrow").offset({
                      top: b.top + 37,
                      left: b.left + a - 15,
                    })
                  : $$("#errorArrow").offset({
                      top: b.top + 3,
                      left: b.left + a - 25,
                    }),
                window.errorArrow(!0))
              : (1 < $$(".section--shipping-method .radio-wrapper").length
                  ? $$("#errorArrow").offset({
                      top: b.top + 38,
                      left: b.left - 35,
                    })
                  : $$("#errorArrow").offset({
                      top: b.top + 4,
                      left: b.left - 35,
                    }),
                window.errorArrow(!1)),
            alert(opcLang.paymentNotice),
            $$(".step__footer button")
              .attr("aria-busy", "false")
              .removeClass("btn--loading"),
            !1)
          : !0;
      }
    );
  }
  function T(a) {
    var b;
    $$(".field--error", a).each(function () {
      $$(this).hasClass("field--email_or_phone")
        ? ($$("div.field--email_or_phone").addClass("field--error"),
          $$("div.field--email_or_phone").find("p").remove(),
          $$("div.field--email_or_phone").append(
            $$(this).find("p")[0].outerHTML
          ),
          H(40, $$(this).find("p").text()))
        : $$(this).attr("data-shopify-pay-email-flow")
        ? ($$("div[data-shopify-pay-email-flow]").addClass("field--error"),
          $$("div[data-shopify-pay-email-flow]").find("p").remove(),
          $$("div[data-shopify-pay-email-flow]").append(
            $$(this).find("p")[0].outerHTML
          ))
        : $$(this).attr("data-address-field")
        ? ((b = $$(this).attr("data-address-field")),
          $$("div[data-address-field='" + b + "']").addClass("field--error"),
          $$("div[data-address-field='" + b + "']")
            .find("p")
            .remove(),
          $$("div[data-address-field='" + b + "']").append(
            $$(this).find("p")[0].outerHTML
          ),
          H(41, $$(this).find("p").text()))
        : $$(this).find(".field__message--error").length
        ? (H(42, $$(this).find(".field__message--error").text()),
          alert($$(this).find(".field__message--error").text()))
        : J("error");
      $$("body,html").animate(
        { scrollTop: $$(".field--error:visible").first().offset().top - 40 },
        400
      );
    });
  }
  function J(a) {
    A || ((A = !0), H(30, a), (window.location.href = "/checkout"));
  }
  function O() {
    var a = !1,
      b = $$(".section--shipping-address").length ? "shipping" : "billing",
      e = $$(".section--" + b + "-address .field--error");
    if (e.length)
      return (
        e.is(":hidden") && e.closest("field").show(),
        $$(
          ".section--" +
            b +
            "-address .field--error input,.section--" +
            b +
            "-address .field--error select"
        ).attr("id")
      );
    [
      "checkout_email",
      "checkout_email_or_phone",
      "checkout_" + b + "_address_first_name",
      "checkout_" + b + "_address_last_name",
      "checkout_" + b + "_address_address1",
      "checkout_" + b + "_address_city",
      "checkout_" + b + "_address_zip",
      "checkout_" + b + "_address_country",
      "checkout_" + b + "_address_phone",
    ]
      .reverse()
      .forEach(function (c) {
        "true" != $$("#" + c + ":visible").attr("aria-required") ||
          $$("#" + c).val() ||
          (a = c);
      });
    0 < $$("#checkout_" + b + "_address_province:visible").length &&
      0 < $$("#checkout_" + b + "_address_province option").length &&
      ($$("#checkout_" + b + "_address_province option:selected").attr(
        "data-alternate-values"
      ) ||
        (a = "checkout_" + b + "_address_province"));
    return a;
  }
  function P() {
    $$(
      ".section--shipping-method .section__content .content-box[data-shipping-methods]"
    ).removeClass("opcErrorBox");
    H(8);
    var a, b, e, c;
    I();
    var d;
    $$("#checkout_email_or_phone").removeAttr("data-autofocus");
    $$("#checkout_email").removeAttr("data-autofocus");
    if (q) {
      var g = $$(".step .edit_checkout").first();
      $$('input[name="checkout[email_or_phone]"]').val(
        $$("#checkout_email_or_phone").val()
      );
    } else g = $$(".edit_checkout[data-shipping-method-form]");
    var h = g.attr("action");
    x = !1;
    $$.ajax({ type: "POST", url: h, data: g.serialize() })
      .done(function (f) {
        function l() {
          x = !0;
          $$(".section--payment-method").show();
          $$(".review-block").parents(".section").hide();
          $$(".section--payment-method input[type=radio]").length &&
            $$(".section--payment-method")
              .find(".content-box__row--secondary")
              .addClass("hidden");
          !1 !== n
            ? ($$(".section--payment-method input.input-radio").removeAttr(
                "checked"
              ),
              $$(".section--payment-method")
                .find("input[value='" + n + "']")
                .attr("checked", "checked")
                .trigger("click"))
            : $$(".section--payment-method input[type=radio]:checked").length
            ? ((n = $$(".section--payment-method")
                .find("input[type=radio]:checked")
                .val()),
              $$(".section--payment-method")
                .find("input[type=radio]:checked")
                .trigger("click"))
            : ($$(".section--payment-method input[type=radio]").removeAttr(
                "checked"
              ),
              $$(".section--payment-method")
                .find(".input-radio")
                .first()
                .attr("checked", "checked")
                .trigger("click"),
              (n = $$(".section--payment-method")
                .find("input[type=radio]:checked")
                .val()),
              $$("div[data-subfields-for-gateway='" + n + "']").removeClass(
                "hidden"
              ));
          $$("div[data-subfields-for-gateway='" + n + "']").removeClass(
            "hidden"
          );
          (1 == opcSettings.design || 1300 > $$(window).width()) &&
            $$("body,html").animate(
              {
                scrollTop:
                  $$(".section--payment-method").first().offset().top - 0,
              },
              400
            );
          var u = 0,
            r = setInterval(function () {
              u++;
              0 < b &&
                0 == $$(window).scrollTop() &&
                ((1 == opcSettings.design || 1300 > $$(window).width()) &&
                  $$("body,html").animate(
                    {
                      scrollTop:
                        $$(".section--payment-method").first().offset().top - 0,
                    },
                    400
                  ),
                clearInterval(r));
              200 === u && clearInterval(r);
            }, 10);
          K();
          U();
          $$(".section--billing-address").show();
          S();
          $$(".section--payment-method .content-box__row--secondary").css({
            display: "table",
            overflow: "visible",
            padding: "1em",
            height: "auto",
            "box-sizing": "border-box",
            width: "100%",
          });
          setTimeout(function () {
            $$("#section--billing-address__different").show();
          }, 4e3);
          H(9);
          L();
          N();
        }
        if (q) {
          if ($$(".field--error", f).length) {
            T(f);
            U();
            return;
          }
          if ($$("#checkout_email_or_phone", f).length) {
            $$("div.field--email_or_phone").addClass("field--error");
            U();
            return;
          }
          if ($$("#checkout_email", f).length) {
            $$("#checkout_email").closest(".field").addClass("field--error");
            U();
            return;
          }
        }
        if ($$(".section--shipping-method .notice--error", f).length)
          (window.location.href = "/checkout"), U();
        else {
          $$("head").find("script").remove();
          var m = f.split("\x3c!--<![endif]--\x3e")[1].split("</head>")[0];
          c = m.match(
            /<script src="\/\/cdn.shopify.com\/s\/assets\/checkout(.*)><\/script>/
          );
          m = m.replace(c[0], "");
          m = m.replace(
            "&quot;client_attributes_checkout&quot;;",
            "'client_attributes_checkout';"
          );
          $$("head").append(m);
          setTimeout(function () {
            $$("head").append(c[0]);
          }, 1700);
          b = $$(window).scrollTop();
          f = f.substring(f.indexOf("<body>") + 6, f.indexOf("</body>"));
          $$("form.edit_checkout .section--payment-method").remove();
          $$("form.edit_checkout input").attr("value", function () {
            return $$(this).val();
          });
          $$("form.edit_checkout select option:checked").attr("checked2", "1");
          $$("form.edit_checkout select option").removeAttr("selected");
          $$("form.edit_checkout select option[checked2='1']")
            .attr("selected", "selected")
            .removeAttr("checked2");
          $$('form.edit_checkout input[type="checkbox"]:checked').attr(
            "checked2",
            "1"
          );
          $$('form.edit_checkout input[type="checkbox"]').removeAttr("checked");
          $$("form.edit_checkout input[checked2='1']")
            .attr("checked", "checked")
            .removeAttr("checked2");
          $$('.section--shipping-method input[type="radio"]').removeAttr(
            "checked"
          );
          $$(".section--shipping-method input[value='" + k + "']").attr(
            "checked",
            "checked"
          );
          a = $$("form.edit_checkout")[0].outerHTML;
          oldSidebar = $$(".sidebar").html();
          oldMainheader = $$(".main__header").html();
          oldFooterA = $$(".step__footer a");
          q || (a += $$(".section--shipping-method")[0].outerHTML);
          $$("body").empty();
          I();
          $$("body").append('<div id="opcWrapper">' + f + "</div>");
          $$(".content").addClass("mb_opc");
          $$(".sidebar").html(oldSidebar);
          $$(".main__header").html(oldMainheader);
          $$(".step__footer a").remove();
          $$(".step__footer").append(oldFooterA);
          0 < $$("body .step__sections .section").length
            ? $$("body .step__sections .section").first().before(a)
            : $$("body .step").first().before(a);
          $$("#section--billing-address__different").hide();
          $$(".review-block").parents(".section").hide();
          q || $$(".section--billing-address").hide();
          L();
          I();
          $$(".breadcrumb").remove();
          window.scrollTo(0, b);
          pa();
          M();
          $$(".section--payment-method .content-box__row--secondary").css({
            display: "block",
            overflow: "hidden",
            padding: "0px",
            height: "0px",
          });
          e = 0;
          var w = setInterval(function () {
            e++;
            if (
              $$("div[data-payment-subform='free']:visible").length ||
              $$("div[data-payment-subform='gift_cards']:visible").length
            )
              clearInterval(w), (d = !0), l();
            if (
              0 < $$(".section--payment-method input").length &&
              (!$$("#card-fields__loading-error:visible").length ||
                $("#card-fields__loading-error:visible").siblings(
                  '.fieldset:not(".hidden")'
                ).length)
            ) {
              var u =
                "#checkout_payment_gateway_" +
                $$("div[data-credit-card-fields]")
                  .parent(".radio-wrapper")
                  .attr("data-subfields-for-gateway");
              ($$(u).is(":checked") &&
                $$("div[data-credit-card-fields]").not(":visible").length &&
                !$$("div[data-credit-card-summary]:visible").length) ||
                (0 < $$("input[name='hosted_fields_redirect']").length
                  ? 150 !== e || d || (clearInterval(w), (d = !0), l())
                  : (clearInterval(w), (d = !0), l()));
            }
            400 !== e ||
              d ||
              (3 < t ? J("payment") : submitFirstStep(!0),
              clearInterval(w),
              (d = !0));
          }, 20);
        }
      })
      .fail(function () {
        setTimeout(function () {
          x ||
            (t++, 3 < t ? J("fail submit second step") : submitFirstStep(!0));
        }, 4e3);
      });
  }
  function N() {
    $$(".policy-list__item a").on("click", function () {
      window.open($$(this).attr("href"));
      return !1;
    });
  }
  function la() {
    setInterval(function () {
      1 < $$(".flag-selector").length &&
        $$(".flag-selector").not(":first").remove();
    }, 300);
  }
  function ka() {
    $$("body").on("click", "button.order-summary-toggle", function () {
      $$("button.order-summary-toggle").removeAttr("data-drawer-toggle");
      $$("#order-summary").hasClass("order-summary--is-collapsed")
        ? ($$("button.order-summary-toggle").addClass(
            "order-summary-toggle--hide"
          ),
          $$("button.order-summary-toggle").removeClass(
            "order-summary-toggle--show"
          ),
          $$(".order-summary__sections").show(),
          $$("#order-summary").removeClass("order-summary--is-collapsed"),
          $$("#order-summary").addClass("order-summary--is-expanded"))
        : ($$("button.order-summary-toggle").addClass(
            "order-summary-toggle--show"
          ),
          $$("button.order-summary-toggle").removeClass(
            "order-summary-toggle--hide"
          ),
          $$("#order-summary").removeClass("order-summary--is-expanded"),
          $$("#order-summary").addClass("order-summary--is-collapsed"));
    });
  }
  function Q(a) {
    var b = a
        ? $$(
            '.step[data-step="payment_method"] button:not(#continue_button):visible'
          ).closest("form")
        : $$(".order-summary__sections form button:visible").closest("form"),
      e = b.find("button");
    e.addClass("btn--loading")
      .attr("aria-busy", "true")
      .attr("disabled", "true");
    $$("#error-for-reduction_code:visible").hide();
    a = b.attr("action");
    $$.ajax({ type: "POST", url: a, data: b.serialize() })
      .done(function (c) {
        e.removeClass("btn--loading")
          .attr("aria-busy", "false")
          .removeAttr("disabled");
        -1 != c.indexOf("error-for-reduction_code")
          ? (b.find(".field").addClass("field--error"),
            0 == $$("#error-for-reduction_code:visible").length
              ? b
                  .find(".field")
                  .append($$("#error-for-reduction_code", c)[0].outerHTML)
              : $$("#error-for-reduction_code:visible").show())
          : ((y = !1), opcLoadCart());
      })
      .fail(function () {
        J("fail submit discount");
      });
  }
  function R(a) {
    t = 0;
    a = a
      ? $$(
          '.step[data-step="payment_method"] button.tag__button:visible'
        ).closest("form")
      : $$(".order-summary__sections form button.tag__button:visible").closest(
          "form"
        );
    a.find("button").hide();
    var b = a.attr("action");
    $$.ajax({ type: "POST", url: b, data: a.serialize() })
      .done(function () {
        y = !1;
        opcLoadCart();
      })
      .fail(function () {
        J("fail cancel discount");
      });
  }
  function oa() {
    function a() {
      H(4);
      O() || (q ? P() : ((t = 0), submitFirstStep()));
    }
    var b,
      e = $$(".section--shipping-address").length ? "shipping" : "billing";
    $$("body").on(
      "input propertychange",
      "#checkout_email_or_phone, #checkout_email, #checkout_phone,.section--" +
        e +
        "-address input",
      function () {
        $$(this).closest(".field--error").removeClass("field--error");
        $$("#errorArrow").hide();
        clearTimeout(b);
        3 == opcSettings.design &&
          $$(
            ".section--shipping-address .field, .section--billing-address .field"
          ).css("height", "56px");
        b = setTimeout(a, 3500);
      }
    );
    $$("body").on(
      "change",
      ".section--" + e + "-address select,.flag-selector select",
      function () {
        $$("#errorArrow").hide();
        clearTimeout(b);
        b = setTimeout(a, 3500);
      }
    );
    $$("body").on("change", ".section--" + e + "-address select", function () {
      $$("#error-for-zip").parent(".field--error").removeClass("field--error");
      $$("#error-for-zip").remove();
    });
  }
  function ia() {
    $$("body").on(
      "change",
      ".section--shipping-method input[type=radio]",
      function () {
        k = this.value;
        $$("div[data-shipping-warning]").remove();
        P();
      }
    );
  }
  function ja() {
    $$("body").on(
      "change",
      ".section--payment-method input.input-radio",
      function () {
        n = this.value;
      }
    );
  }
  function L() {
    $$(".step__footer").css("display", "block");
    $$(".step__footer button").css("width", "100%");
    $$(".step__footer button span").text(opcLang.completeOrder);
    $$(".step__footer .shown-if-js").css("width", "100%");
    if (
      1300 > $$(window).width() &&
      opcSettings.boosters &&
      1 == opcSettings.boosters.booster5
    ) {
      $$(".step__footer").css("position", "fixed");
      $$(".step__footer").css("overflow", "hidden");
      $$(".step__footer").css("bottom", "0");
      $$(".step__footer").css("left", "0");
      $$(".step__footer").css("width", "100%");
      $$(".step__footer__previous-link").hide();
      var a = 0;
      $$(document).scroll(function () {
        setTimeout(function () {
          a = $$("#guaranteeImgBtn").length
            ? $$(document).height() -
              140 -
              $$("#guaranteeImgBtn").outerHeight(!0)
            : $$(document).height() - 140;
          $$(window).scrollTop() + $$(window).height() > a ||
          !$$(".ship_step_wait").length
            ? ($$(".step__footer").css("position", "relative"),
              $$(".step__footer__previous-link").show(),
              $$("#stepFooterHolder").remove())
            : ($$("#stepFooterHolder").length ||
                ($$(".step__footer").after('<div id="stepFooterHolder"></div>'),
                $$("#stepFooterHolder").css("width", "100%"),
                $$("#stepFooterHolder").css(
                  "height",
                  $$(".step__footer").outerHeight(!0) + "px"
                )),
              $$(".step__footer").css("position", "fixed"),
              $$(".step__footer").css("bottom", "0"),
              $$(".step__footer").css("left", "0"),
              $$(".step__footer").css("width", "100%"),
              $$(".step__footer__previous-link").hide());
        }, 500);
      });
    }
  }
  function ma() {
    if (opcSettings.upsells) {
      var a = 1e4,
        b = "",
        e = [];
      $$(".product").each(function () {
        e.push($$(this).attr("data-product-id"));
      });
      opcSettings.upsells.forEach(function (c) {
        c.trigger.split(",").forEach(function (d) {
          -1 != e.indexOf(d) &&
            c.priority < a &&
            ((a = c.priority), (b = c.products.split(",")));
        });
      });
      $$.getJSON("https://" + Shopify.shop + "/products.json").done(function (
        c
      ) {
        c.products.forEach(function (d) {
          d.variants.forEach(function (g) {
            if (-1 != b.indexOf(g.id.toString())) {
              if (!$$("#opcUpsell").length) {
                var h =
                  '<div id="opcUpsell" style="padding: 0 8px 7px;/*background: #fafafa;border: 1px solid #e8e8e8;*/border-radius: 7px;margin-top: 26px;"><div class="section__header"><h2 class="section__title" style="margin-bottom:0px;font-size:17px;">' +
                  opcLang.upsellTitle +
                  '</h2></div><div><table class="product-table">    <tbody data-order-summary-section="line-items">    </tbody></table></div></div>';
                $$("body").append(
                  '<div id="opcUpsellPopup" style="position:fixed;left:0;right:0;top:50px;z-index:99998;padding-top:50px;background-color:white;margin: auto;padding: 15px;border: 1px solid #c2c2c2;box-shadow:0px 0px 14px 0px rgba(0,0,0,0.75);width: 80%;max-width:500px;"><span class="close" onclick="document.getElementById(\'opcUpsellPopup\').style.display = \'none\';return false;" style="cursor:pointer;color: #aaaaaa;position:absolute;top:10px;right:10px;font-size: 40px;font-weight:bold;">&times;</span></div>'
                );
                $$("#opcUpsellPopup").prepend(h);
                $$("#opcUpsellPopup").fadeIn();
                -1 != window.location.search.indexOf("upsell=1") &&
                  $$("#opcUpsell .section__header").prepend(
                    "<p style='font-size:17px;font-weight:bold;'>" +
                      opcLang.upsellAdded +
                      " &#x2714;</p><br>"
                  );
              }
              h =
                0 < d.images.length
                  ? '<img class="product-thumbnail__image" src="' +
                    d.images[0].src +
                    '">'
                  : "";
              if (S())
                var f =
                    '<span class="order-summary__emphasis">' +
                    g.price +
                    " " +
                    Shopify.currency.active +
                    "</span>",
                  l = "";
              else
                (f = ""),
                  (l =
                    '<td class="product__price">  <span class="order-summary__emphasis">' +
                    g.price +
                    " " +
                    Shopify.currency.active +
                    "</span></td>");
              g =
                '<tr class="product" data-product-type="" data-customer-ready-visible="">    <td class="product__image">        <div class="product-thumbnail ">            <div class="product-thumbnail__wrapper">' +
                h +
                '</div>        </div>    </td>    <th class="product__description" scope="row">        <span class="product__description__name order-summary__emphasis">' +
                d.title +
                '</span>        <span class="product__description__variant order-summary__small-text"></span>       ' +
                f +
                "    </th>  " +
                l +
                '   <td class="product__price">        <button name="button" type="button" onclick="opcAddToCart(this,' +
                g.id +
                ')" class="field__input-btn btn" aria-busy="false" style="padding: 10px 5px;">' +
                opcLang.addToCheckout +
                "</button>    </td></tr>";
              $$("#opcUpsell tbody").prepend(g);
            }
          });
        });
      });
    }
  }
  function na() {
    if (opcSettings.boosters) {
      1 == opcSettings.boosters.booster2 &&
        (document.querySelector(".discountMinimize") ||
          !$$("#checkout_reduction_code:visible").length ||
          $$(".order-summary-toggle:visible").length ||
          ((document
            .querySelector(
              ".sidebar__content input[name='checkout[reduction_code]']"
            )
            .closest(".field").style.display = "none"),
          $$(".sidebar__content input[name='checkout[reduction_code]']")
            .closest(".fieldset")
            .append(
              '<p class="discountMinimize" onclick="document.querySelector(&quot;.sidebar__content input[name=&apos;checkout[reduction_code]&apos;]&quot;).closest(&apos;.field&apos;).style.display = &apos;&apos;;document.querySelector(&quot;.discountMinimize&quot;).style.display = \'none\';" style="cursor:pointer;"><span style="display:inline-block;margin-right:3px;width: 10px;height: 10px;border: 1px solid grey;border-radius: 50%;font-size: 13px;line-height: 7px;text-align: center;background: #484848;color: white;">&#43;</span><span style="display:inline-block;">Discount code?</span></p>'
            )),
        document.querySelector(".addressMinimize") ||
          !$$("#checkout_shipping_address_address2:visible").length ||
          $$(".order-summary-toggle:visible").length ||
          ((document.querySelector(
            "div[data-address-field='address2'] input"
          ).style.display = "none"),
          (document.querySelector(
            "div[data-address-field='address2'] label"
          ).style.display = "none"),
          $$("#checkout_shipping_address_address2")
            .closest(".field__input-wrapper")
            .append(
              '<p class="addressMinimize" onclick="document.querySelector(&quot;div[data-address-field=&apos;address2&apos;] input&quot;).style.display = &apos;&apos;;document.querySelector(&quot;div[data-address-field=&apos;address2&apos;] label&quot;).style.display = &apos;&apos;;document.querySelector(&quot;.addressMinimize&quot;).style.display = &apos;none&apos;;" style="cursor:pointer;"><span style="display:inline-block;margin-right:3px;width: 10px;height: 10px;border: 1px solid grey;border-radius: 50%;font-size: 13px;line-height: 7px;text-align: center;background: #484848;color: white;">&#43;</span><span style="display:inline-block;">Address line 2</span></p>'
            )),
        $$(".section--shipping-address").length &&
          !$$(".merged").length &&
          ($$("div[data-section='customer-information']")
            .find(".fieldset")
            .prependTo(".section--shipping-address .section__content"),
          $$(".fieldset").css("clear", "both"),
          $$("div[data-section='customer-information']").addClass("merged"),
          $$("#checkout_remember_me")
            .closest(".checkbox-wrapper")
            .replaceWith($$("div[data-buyer-accepts-marketing]")),
          $$(".section--contact-information").parent().remove(),
          $$(".section--shipping-address h2").text(
            $$(".section--shipping-address h2").text().replace("2. ", "1. ")
          ),
          $$(".section--shipping-method h2").text(
            $$(".section--shipping-method h2").text().replace("3. ", "2. ")
          ),
          $$(".section--payment-method h2").text(
            $$(".section--payment-method h2").text().replace("4. ", "3. ")
          )));
      if (1 == opcSettings.boosters.booster3) {
        var a = opcSettings.boosters.booster3_heading,
          b = opcSettings.boosters.booster3_title1,
          e = opcSettings.boosters.booster3_text1,
          c = opcSettings.boosters.booster3_img1;
        "" != c && (c = '<img src="' + c + '" />');
        var d = opcSettings.boosters.booster3_title2,
          g = opcSettings.boosters.booster3_text2,
          h = opcSettings.boosters.booster3_img2;
        "" != h && (h = '<img src="' + h + '" />');
        var f = opcSettings.boosters.booster3_title3,
          l = opcSettings.boosters.booster3_text3,
          m = opcSettings.boosters.booster3_img3;
        "" != m && (m = '<img src="' + m + '" />');
        var w = opcSettings.boosters.booster3_title4,
          u = opcSettings.boosters.booster3_text4,
          r = opcSettings.boosters.booster3_img4;
        "" != r && (r = '<img src="' + r + '" />');
        $$(".sidebar__content .order-summary__sections").length &&
          !$$("#booster3").length &&
          ($$(".order-summary__sections").append(
            '<div id="booster3" style="padding-bottom:20px;"><div id="booster3Title"><span>' +
              a +
              '</span></div><div class="booster3Item">' +
              c +
              "<div><h4>" +
              b +
              "</h4><p>" +
              e +
              '</p></div></div><div class="booster3Item">' +
              h +
              "<div><h4>" +
              d +
              "</h4><p>" +
              g +
              '</p></div></div><div class="booster3Item">' +
              m +
              "<div><h4>" +
              f +
              "</h4><p>" +
              l +
              '</p></div></div><div class="booster3Item">' +
              r +
              "<div><h4>" +
              w +
              "</h4><p>" +
              u +
              "</p></div></div></div>"
          ),
          (a = document.getElementById("booster3Title")),
          (style = document.createElement("style")),
          a.appendChild(style),
          (style.type = "text/css"),
          style.appendChild(
            document.createTextNode(
              '#booster3Title{margin-top:25px;overflow:hidden;text-align:center;font-size:16px}#booster3Title span{position:relative;}#booster3Title span:before,#booster3Title span:after {content: "";position: absolute;height: 5px;border-bottom: 1px solid;border-top: 1px solid;top: 8px;width: 600px;}#booster3Title span:before {right: 100%;margin-right: 15px;}#booster3Title span:after{left: 100%;margin-left: 15px;}.booster3Item{margin-top:30px;font-size:15px;}.booster3Item p{line-height:1.2;}.booster3Item h4{font-weight:bold;}.booster3Item img{width:13%;float:left;}.booster3Item div{width:75%;margin-left:25%}.booster3Item::after{content:"";clear:both;height:0;display:block;}'
            )
          ));
      }
      if (1 == opcSettings.boosters.booster4) {
        var qa =
          '<div id="guaranteeImgBtn" style="display: block;width: 100%;text-align: center;padding-top: 24px;clear: both;margin-bottom: -38px;"><img src="' +
          opcSettings.boosters.booster4_img +
          '"></div>';
        setInterval(function () {
          !$$("#guaranteeImgBtn").length &&
            $$(".section--payment-method").length &&
            $$(".step__footer").after(qa);
        }, 1e3);
      }
      1 == opcSettings.boosters.booster1 &&
        ((a =
          '<div id="booster1" style="display:block;background:#fffde4;padding:8px 18px;margin:10px 0;font-size:13px;color:#2c2c2c;font-weight:bold;border:1px solid #e3da74;-webkit-border-radius: 4px;-moz-border-radius: 4px;border-radius: 4px;">' +
          opcSettings.boosters.booster1_text +
          "</div>"),
        $$(".main__header").append(a),
        (function () {
          if (V("opcTimer")) var B = (V("opcTimer") - Date.now()) / 1e3;
          else
            (B = 60 * opcSettings.boosters.booster1_min),
              W("opcTimer", Date.now() + 1e3 * B, 1);
          var C,
            D,
            ra = setInterval(function () {
              0 >= B
                ? ($$("#booster1").text(opcSettings.boosters.booster1_text2),
                  clearInterval(ra))
                : ((C = parseInt((B / 60) % 60, 10)),
                  (D = parseInt(B % 60, 10)),
                  (C = 10 > C ? "0" + C : C),
                  (D = 10 > D ? "0" + D : D),
                  $$("#timer").text(C + ":" + D),
                  B--);
            }, 1e3);
        })());
    }
  }
  function I() {
    p < $$(document).height() && (p = $$(document).height());
    $$(".main").css("min-height", p);
    $$("#cartLoader").length
      ? $$("#cartLoader").css("min-height", $$(document).height())
      : $$("body").append(
          '<div id="cartLoader" style="left:0;top:0;height:' +
            p +
            'px;width:100%;text-align:center;background-color: rgba(255, 255, 255, .5);position:absolute;z-index:9999999999"><img style="position:fixed;top:70px;left:50%;margin-left:-100px;" src="https://d17awlyy7mou9o.cloudfront.net/shopify/images/opc_loader.gif" /></div>'
        );
  }
  function U() {
    z = 0;
    $$("#cartLoader").remove();
    $$(".main").css("min-height", "auto");
  }
  function ca() {
    setInterval(function () {
      $$("#cartLoader").length && (z++, 36 < z && J("cartloader timeout"));
    }, 500);
  }
  function sa() {
    var a = navigator.userAgent || navigator.vendor || window.opera;
    return (
      -1 < a.indexOf("FBAN") ||
      -1 < a.indexOf("FBAV") ||
      -1 < a.indexOf("Instagram")
    );
  }
  function X() {
    if (
      ta() ||
      -1 <
        navigator.userAgent.indexOf("Mozilla/5.0 (Macintosh; Intel Mactest") ||
      (-1 < navigator.userAgent.indexOf("Mozilla/5.0 (iPhonetest;") &&
        -1 < navigator.userAgent.indexOf("Safari"))
    )
      return !1;
    var a = navigator.userAgent.toLowerCase();
    if (
      -1 < a.indexOf("chrome") ||
      -1 < a.indexOf("fban") ||
      -1 < a.indexOf("fbav") ||
      -1 < a.indexOf("instagram") ||
      -1 < a.indexOf("safari")
    )
      return !0;
    if (-1 < a.indexOf("firefox")) {
      var b = window.navigator.userAgent.match(/Firefox\/([0-9]+)\./);
      if (66 < (b ? parseInt(b[1]) : 0)) return !0;
    }
    return -1 < a.indexOf("samsungbrowser") ? !0 : !1;
  }
  function ta() {
    var a = window.navigator.userAgent,
      b = a.indexOf("MSIE ");
    if (0 < b) return parseInt(a.substring(b + 5, a.indexOf(".", b)), 10);
    b = a.indexOf("Edge/");
    return 0 < b
      ? parseInt(a.substring(b + 5, a.indexOf(".", b)), 10)
      : 0 < a.indexOf("Trident/")
      ? ((b = a.indexOf("rv:")),
        parseInt(a.substring(b + 3, a.indexOf(".", b)), 10))
      : !1;
  }
  function F() {
    if (opcSettings.customJsFile) {
      var a = document.createElement("SCRIPT");
      a.src = decodeURIComponent(opcSettings.customJsFile);
      a.type = "text/javascript";
      document.getElementsByTagName("head")[0].appendChild(a);
    }
  }
  function ua() {
    if (opcSettings.customCssFile) {
      var a = document.head,
        b = document.createElement("link");
      b.type = "text/css";
      b.rel = "stylesheet";
      b.href = decodeURIComponent(opcSettings.customCssFile);
      a.appendChild(b);
    }
  }
  function va(a) {
    return (a = a.replace(
      "document.getElementsByTagName('head')[0].appendChild(eventsListenerScript)",
      ""
    ));
  }
  function fa() {
    if ("undefined" !== typeof fbq && $$(".payment-due__price").length) {
      var a = [],
        b = 0,
        e = $$(".order-summary__section__content .product").length,
        c = $$(".payment-due__price").attr("data-checkout-payment-due-target");
      c = parseFloat(c.substring(0, c.length - 2) + "." + c.slice(-2));
      $$(".order-summary__section__content .product").each(function () {
        a.push($$(this).attr("data-product-id"));
        b += parseInt($$(this).find(".product__quantity").text());
      });
      fbq("track", "InitiateCheckout", {
        c: e,
        value: c,
        currency: Shopify.Checkout.currency,
        a: a,
        b: "product_group",
      });
    }
  }
  function wa() {
    var a = 0;
    if (
      -1 != navigator.userAgent.indexOf("Windows NT 10") ||
      -1 != navigator.userAgent.indexOf("Edge")
    )
      a = 50;
    var b = !1,
      e = !1;
    $$(document).mouseout(function (c) {
      var d = c.relatedTarget || c.toElement;
      1 < c.pageY && (b = !0);
      if (
        (!d || "HTML" == d.nodeName || 0 > c.pageY - $$(window).scrollTop()) &&
        0 == e &&
        c.pageY - a < $$(window).scrollTop() &&
        b
      )
        return (
          (e = !0),
          (c =
            '<div id="opcPopup" style="position:fixed;z-index:99999;padding-top:50px;left:0;top:0;width:100%;height:100%;overflow:auto;background-color: rgba(0,0,0,0.5)"><div style="position:relative;background-color: ' +
            opcSettings.popupBkg +
            ';margin: auto;padding: 15px;border: 1px solid #888;width: 80%;max-width:600px;"><span class="close" onclick="document.getElementById(\'opcPopup\').style.display = \'none\';return false;" style="cursor:pointer;color: #aaaaaa;position:absolute;top:10px;right:10px;font-size: 40px;font-weight:bold;">&times;</span>' +
            opcSettings.popupHtml +
            "</div></div>"),
          $$("#opcPopup").length || $$("body").append(c),
          $$("#opcPopup").show(),
          !1
        );
    });
  }
  function Y(a) {
    for (
      var b = null, e, c = location.search.substr(1).split("&"), d = 0;
      d < c.length;
      d++
    )
      (e = c[d].split("=")), e[0] === a && (b = decodeURIComponent(e[1]));
    return b;
  }
  function H(a, b) {
    if (V("opc_v")) {
      var e = new Image();
      (b && "undefined" != b) || (b = "");
      if (4 == a) {
        var c = $$(".section--shipping-address").length
            ? "shipping"
            : "billing",
          d = "";
        [
          "checkout_email",
          "checkout_email_or_phone",
          "checkout_" + c + "_address_first_name",
          "checkout_" + c + "_address_last_name",
          "checkout_" + c + "_address_address1",
          "checkout_" + c + "_address_city",
          "checkout_" + c + "_address_zip",
          "checkout_" + c + "_address_country",
          "checkout_" + c + "_address_phone",
        ].forEach(function (l) {
          "true" == $$("#" + l + ":visible").attr("aria-required") &&
            $$("#" + l).val() &&
            (d = d + "," + l);
        });
        0 < $$("#checkout_" + c + "_address_province:visible").length &&
          0 < $$("#checkout_" + c + "_address_province option").length &&
          $$("#checkout_" + c + "_address_province option:selected").attr(
            "data-alternate-values"
          ) &&
          (d = d + ",checkout_" + c + "_address_province");
        e.src =
          "https://www.magebird.com/shopify/trk.gif?u=" +
          V("opc_v") +
          "&sp=" +
          a +
          "&rand=" +
          Math.floor(10001 * Math.random()) +
          "&error=" +
          b +
          "&fields=" +
          d;
      } else
        e.src =
          "https://www.magebird.com/shopify/trk.gif?u=" +
          V("opc_v") +
          "&sp=" +
          a +
          "&rand=" +
          Math.floor(10001 * Math.random()) +
          "&error=" +
          b;
    } else {
      c = V("is_opc");
      W("opc_v", Math.random().toString(36).substring(4), 60);
      var g = window.location.hostname,
        h = window.innerWidth,
        f = navigator.userAgent;
      e = new Image();
      e.src =
        "https://www.magebird.com/shopify/trk.gif?sp=" +
        a +
        "&isOpc=" +
        c +
        "&u=" +
        V("opc_v") +
        "&store=" +
        g +
        "&scr_width=" +
        h +
        "&browser=" +
        f +
        "&rand=" +
        Math.floor(10001 * Math.random());
    }
  }
  function V(a) {
    a += "=";
    for (var b = document.cookie.split(";"), e = 0; e < b.length; e++) {
      for (var c = b[e]; " " == c.charAt(0); ) c = c.substring(1);
      if (-1 != c.indexOf(a))
        return decodeURIComponent(c.substring(a.length, c.length));
    }
    return "";
  }
  function W(a, b, e) {
    var c = new Date();
    c.setTime(c.getTime() + 864e5 * e);
    document.cookie = a + "=" + b + "; expires=" + c.toUTCString() + "; path=/";
  }
  function pa() {
    $$(".section--contact-information h2").length
      ? q
        ? $$(".section--payment-method h2").text(
            "3. " + $$(".section--payment-method h2").text()
          )
        : $$(".section--payment-method h2").text(
            "4. " + $$(".section--payment-method h2").text()
          )
      : q
      ? $$(".section--payment-method h2").text(
          "2. " + $$(".section--payment-method h2").text()
        )
      : $$(".section--payment-method h2").text(
          "3. " + $$(".section--payment-method h2").text()
        );
  }
  function S() {
    return 1300 > $$(window).width() ? !0 : !1;
  }
  window.errorArrow = function (a) {
    a = void 0 === a ? !1 : a;
    $$("#errorArrow")
      .animate({ marginLeft: "-=10px" }, 800)
      .animate({ marginLeft: "+=10px" }, 800);
    setTimeout("window.errorArrow(" + a + ")", 1600);
  };
  window.opcLoadCart = function (a) {
    a = void 0 === a ? !1 : a;
    var b = null;
    Y("discount")
      ? (b = Y("discount"))
      : $$(".cart-form input[name='discount']").val()
      ? (b = $$(".cart-form input[name='discount']").val())
      : $$("form input[name='discount']").val() &&
        (b = $$("form input[name='discount']").val());
    if (
      (sa() ||
        (-1 == navigator.userAgent.indexOf("Chrome") &&
          -1 < navigator.userAgent.indexOf("Safari"))) &&
      window.location.pathname != "/" + opcSettings.checkoutUrl
    )
      setTimeout(function () {
        U();
        window.location.href = b
          ? "undefined" != typeof Shopify && Shopify.locale
            ? "/" +
              opcSettings.checkoutUrl +
              "?discount=" +
              b +
              "&locale=" +
              Shopify.locale
            : "/" + opcSettings.checkoutUrl + "?discount=" + b
          : "undefined" != typeof Shopify && Shopify.locale
          ? "/" + opcSettings.checkoutUrl + "?locale=" + Shopify.locale
          : "/" + opcSettings.checkoutUrl;
      }, 100);
    else {
      I();
      var e = window.location.origin + "/checkout?step=contact_information";
      a && (e = a);
      b && (e = e + "&discount=" + b);
      window.Weglot && Weglot.getCurrentLang()
        ? (e = e + "&locale=" + Weglot.getCurrentLang())
        : "undefined" != typeof Shopify && Shopify.locale
        ? (e = e + "&locale=" + Shopify.locale)
        : Y("locale") && (e = e + "&locale=" + Y("locale"));
      H(2);
      $$.ajax({ type: "GET", url: e })
        .done(function (c, d, g) {
          "" == c && 0 == y
            ? (t++,
              g.getResponseHeader("content-location") &&
              1 == t &&
              -1 == g.getResponseHeader("content-location").indexOf("cart")
                ? opcLoadCart(g.getResponseHeader("content-location"))
                : J("load cart empty data"))
            : -1 != c.indexOf('http-equiv="refresh"')
            ? (v++,
              2 < v
                ? J("redirect refresh")
                : ((c = c.match(
                    /http-equiv="refresh" content="0; url=(.*)">/
                  )[1]),
                  opcLoadCart(c)))
            : 0 == y &&
              ((y = !0),
              (c = va(c)),
              document.write(
                '<img id="opcPreLoader" style="position:fixed;z-index:9;top:70px;left:50%;margin-left:-100px;" src="https://cdn.shopify.com/s/files/1/0052/2377/6291/files/opc_loader.gif?16" />'
              ),
              (document.body.style.overflowY = "scroll"),
              (c = c.replace(
                "&quot;client_attributes_checkout&quot;;",
                "'client_attributes_checkout';"
              )),
              1 == c.split("</body>").length - 1 &&
                1 == c.split("<body>").length - 1 &&
                ((c = c.replace("<body>", "<body><div id='opcWrapper'>")),
                (c = c.replace("</body>", "</div></body>")),
                (c = c.replace(
                  "section--contact-information",
                  "section--contact-information2"
                ))),
              document.write(c),
              document.close(),
              setTimeout(function () {
                document.getElementById("opcPreLoader") &&
                  (document.getElementById("opcPreLoader").outerHTML = "");
              }, 8e3),
              ua(),
              (document.title = "Pragiti - Single page checkout"),
              K(),
              ea(),
              opcSettings.enablePopup && wa(),
              da());
        })
        .fail(function () {
          J("load cart fail");
        });
    }
  };
  window.opcAddToCart = function (a, b) {
    $$(a).prop("disabled", !0);
    H(50);
    $$.ajax({
      type: "POST",
      url: "/cart/add.js",
      data: { quantity: 1, id: b },
      dataType: "json",
      success: function () {
        window.location.href = "/" + opcSettings.checkoutUrl + "?upsell=1";
      },
      error: function (e) {
        try {
          alert(JSON.parse(e.responseText).description);
        } catch (c) {
          alert(
            "Oops! Something went wrong. Please try to add your product again. If this message persists, the item may not be available."
          );
        }
      },
    });
  };
  window.submitFirstStep = function (a) {
    a
      ? H(32)
      : setTimeout(function () {
          H(5);
        }, 500);
    var b = 0,
      e = !1,
      c = $$("form.edit_checkout").first();
    if ($$(".field--email_or_phone input[name='null']").length) {
      var d =
          "+" +
          $$(".flag-selector__select option:selected")
            .text()
            .split("+")
            .pop()
            .split(")")[0],
        g = "";
      $$(".field--email_or_phone input[name='null']").each(function () {
        d && -1 != $$(this).val().indexOf(d) && (g = $$(this).val());
      });
      g && $$('input[name="checkout[email_or_phone]"]').val(g);
    }
    $$("#checkout_email_or_phone").length &&
      $$("#checkout_shipping_address_phone").not(":visible").length &&
      $$("#checkout_shipping_address_phone").val(
        $$("#checkout_email_or_phone").val()
      );
    var h = c.attr("action");
    I();
    $$.ajax({ type: "POST", url: h, data: c.serialize() })
      .done(function (f) {
        function l() {
          S();
          $$(".edit_checkout[data-shipping-method-form]")
            .find(".content-box__row--secondary:not(:first)")
            .addClass("hidden");
          $$("div[data-shipping-rate-additional-fields-container]").show();
          $$(".section--billing-address").remove();
          $$(".section--payment-method").remove();
          $$(".step__footer").before(E);
          pa();
          H(6);
          if (a)
            !1 !== k
              ? ($$(".section--shipping-method input[type=radio]").removeAttr(
                  "checked"
                ),
                $$(".edit_checkout[data-shipping-method-form]")
                  .find("input[value='" + k + "']")
                  .attr("checked", "checked")
                  .trigger("click"))
              : $$(".section--shipping-method input[type=radio]:checked")
                  .length &&
                ((k = $$(".edit_checkout[data-shipping-method-form]")
                  .find("input[type=radio]:checked")
                  .val()),
                $$(".edit_checkout[data-shipping-method-form]")
                  .find("input[type=radio]:checked")
                  .trigger("click")),
              P();
          else {
            $$(".section--shipping-method input[type=radio]:checked").prop(
              "checked",
              !1
            );
            var r = !1;
            $$("body").on(
              "click",
              ".section--shipping-method input[type=radio]",
              function () {
                r = !0;
              }
            );
            opcSettings.boosters &&
              1 == opcSettings.boosters.booster6 &&
              1 == $$(".section--shipping-method .radio-wrapper").length &&
              ($$(".edit_checkout[data-shipping-method-form]")
                .find("input[type=radio]")
                .trigger("click"),
              (k = $$(".edit_checkout[data-shipping-method-form]")
                .find("input[type=radio]")
                .val()),
              (r = !0));
            setTimeout(function () {
              r ||
                $$(".section--shipping-method input[type=radio]:checked").prop(
                  "checked",
                  !1
                );
            }, 3e3);
            setTimeout(function () {
              r ||
                $$(".section--shipping-method input[type=radio]:checked").prop(
                  "checked",
                  !1
                );
            }, 5e3);
            (1 == opcSettings.design || 1300 > $$(window).width()) &&
              $$("body,html").animate(
                {
                  scrollTop:
                    $$(".section--shipping-method").first().offset().top - 40,
                },
                400
              );
          }
          K();
          U();
        }
        t++;
        if ($$(".field--error", f).length) T(f), U();
        else {
          f = $$(".step .edit_checkout", f);
          var m = null;
          f.find(".section__header p").length &&
            (m = f.find(".section__header p"));
          f.find(".section__header").remove();
          f.find(".step__footer").remove();
          f.find(".step__sections .section").first().remove();
          $$(".section--shipping-method .section__content").remove();
          $$("#shipping").remove();
          $$(".section--shipping-method").append(
            "<div id='shipping'>" + f[0].outerHTML + "</div>"
          );
          $$(".section--shipping-method .section__header p").remove();
          m &&
            $$(".section--shipping-method .section__header").append(
              m[0].outerHTML
            );
          f.find(".content-box__row--secondary").hide();
          if (
            1 ==
            $$(
              "#shipping .section--shipping-method .blank-slate:not([data-poll-refresh])"
            ).length
          )
            clearInterval(u), (e = !0), U();
          else if (0 == $$("#shipping .section--shipping-method input").length)
            var w = 100,
              u = setInterval(function () {
                b++;
                (0 < $$("#shipping .section--shipping-method input").length ||
                  $$(
                    "#shipping .section--shipping-method .blank-slate:not([data-poll-refresh])"
                  ).length) &&
                  !e &&
                  (clearInterval(u), (e = !0), l());
                1 < t && (w = 200);
                b !== w ||
                  e ||
                  (clearInterval(u),
                  (e = !0),
                  3 > t
                    ? (console.log("new try"), H(31), submitFirstStep())
                    : J("shipping"));
              }, 20);
          else l();
        }
      })
      .fail(function () {
        t++;
        3 > t ? (H(33), submitFirstStep()) : J("fail submit first step");
      });
  };
  if (
    !V("is_opc") &&
    -1 == document.location.href.indexOf("thank_you") &&
    "undefined" != typeof opcSettings &&
    opcSettings.enabled
  ) {
    var Z = Math.floor(2 * Math.random()) ? 1 : 2;
    Z = 1;
    X() || (Z = 2);
    W("is_opc", Z, 60);
  }
  (function () {
    if (V("is_opc"))
      if (
        -1 < window.location.href.indexOf("checkout") &&
        -1 < document.location.href.indexOf("thank_you")
      )
        H(10);
      else if ("function" == typeof $) {
        var a = !1;
        setInterval(function () {
          a ||
            (!$(".popup-cart:visible").length &&
              !$(".cart-popup-content:visible").length) ||
            ((a = !0), H(1));
        }, 300);
        2 != V("is_opc") ||
          a ||
          $.each(
            ".opcAdd;.opcQuickAdd;form button[name='add'];.ProductForm__AddToCart; .cart_buy_button;form input[value='Checkout'];form input[name='checkout'];form input[name='checkout'] *;form button[name='checkout'];form button[name='checkout'] *;button:contains('Checkout');button:contains('checkout');button:contains('CHECKOUT')".split(
              ";"
            ),
            function (b, e) {
              $(document).on("click", e, function () {
                a = !0;
                H(1);
              });
            }
          );
        -1 < window.location.href.indexOf("cart") &&
          $("form[action='/cart']").length &&
          H(1);
        "/cart" == window.location.pathname &&
          (0 == $("form[action*='cart']").length ||
            $(".cart-empty:visible").length ||
            $(".cart--empty-message:visible").length ||
            $(".cart--empty:visible").length) &&
          2 == V("is_opc") &&
          H(30, "load cart empty data no opc");
      }
  })();
  "undefined" != typeof opcSettings &&
    1 == V("is_opc") &&
    opcSettings.enabled &&
    X() &&
    ba();
  "undefined" == typeof opcSettings ||
    -1 == window.location.pathname.indexOf(opcSettings.checkoutUrl) ||
    (X() && opcSettings.enabled) ||
    (Y("discount")
      ? (window.location.href = "/checkout?discount=" + Y("discount"))
      : (window.location.href = "/checkout"));
})();
