srpApp.AppURL = "//ssvnodemean.herokuapp.com";
srpApp.srp_page = 'sample-request-lists';
srpApp.getPageType = function() {
    var url = window.location.toString();
    if (url.match(/\/products\//) !== null || url.match(/\/products_preview/) !== null) {
        return 'product'
    } else if (url.match(/\/cart/) !== null) {
        return 'cart'
    } else if (url.match(/\/collections\//) !== null) {
        return 'collection'
    } else if (url.match(/\/pages\//) !== null) {
        if (url.indexOf(srpApp.srp_page) !== -1)
            return 'srplists_page';
        else
            return 'page'
    } else if (url.match(/\/search\?/) !== null) {
        return 'search'
    } else {
        return ''
    }
}
srpApp.getLists = function(){
    var requestUrl = srpApp.AppURL+"/getsamplerequests";
    var sampleRequestData;
     $.ajax({
            url: requestUrl,
            jsonp: 'callback',
            dataType: "jsonp",
            success: function (data) {
                sampleRequestData = data;
                jQuery.each(data, function (index, item) {
                    console.log('index= ' + index);
                    console.log('item= ' + item);
                    console.log('item name= ' + item.name);
                    console.log('item email= ' + item.email);
                    var rowData = '<tr><td>' + item.date + '</td><td>' + item.email + '</td><td>' +
                        item.name +
                        '</td><td><a data-index="' + index +
                        '" class="js-view-srplink"  href="#viewRspDetail">View details</a></td></tr>';
                    jQuery('#ajaxResponse tbody').append(rowData);
                });
                srplink();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error ' + textStatus + " " + errorThrown);
            }
        });

        function srplink() {
            console.log('srplink inside');
            jQuery('.js-view-srplink').magnificPopup({
                type: 'inline',
                midClick: true,
                callbacks: {
                    beforeOpen: function () {
                        var thisEle = jQuery(this.st.el);
                        var thisIndex = jQuery(thisEle).attr('data-index');
                        var id = sampleRequestData[thisIndex]._id;
                        var date = sampleRequestData[thisIndex].date;
                        var name = sampleRequestData[thisIndex].name;
                        var email = sampleRequestData[thisIndex].email;
                        var msg = sampleRequestData[thisIndex].message;
                        var pid = sampleRequestData[thisIndex].product.id;
                        var pimg = sampleRequestData[thisIndex].product.img;
                        var pname = sampleRequestData[thisIndex].product.name;
                        var pprice = sampleRequestData[thisIndex].product.price;
                        var pvid = sampleRequestData[thisIndex].product.vid;
                        modalShowData(id, date, name, email, msg, pid, pimg, pname, pprice, pvid);
                    }
                }
            });
        }

        function modalShowData(id, date, name, email, msg, pid, pimg, pname, pprice, pvid) {
            jQuery('#srpDetailId').text(id);
            jQuery('#srpDetailDate').text(date);
            jQuery('#srpDetailName').text(name);
            jQuery('#srpDetailEmail').text(email);
            jQuery('#srpDetailMsg').text(msg);
            jQuery('#srpDetailId').text(pid);
            jQuery('#srpDetailPimg').attr('src', pimg);
            jQuery('#srpDetailPname').text(pname);
            jQuery('#srpDetailPprice').text(pprice);
            jQuery('#srpDetailPname').attr('data-variant-id', pvid);
        }
};
if (srpApp.getPageType() == 'srplists_page') {
    srpApp.getLists();
}
// alert('sample request app');
//     $('#srpForm').submit(function(e) {
//         e.preventDefault();
//         alert('srpForm submit');
//         data = $('form#srpForm').serialize();
//         $.ajax({
//             url: $('#srpForm').attr('action'),
//             type: "POST",
//             dataType: 'json',
//             jsonp: 'callback',
//             cache: !1,
//             contentType: !1,
//             processData: !1,
//             data: data,
//             crossDomain: !0,
//             success: function(result) {
//                 alert('srpForm succcess');
//                 console.log('srpForm succcess');
//             },
//             complete: function(data) {
//                 console.log('complete data ' + data);
//             },
//             error: function (jqXHR, textStatus, errorThrown) {
//                 alert('error ' + textStatus + " " + errorThrown);
//             }
//         });
// });
var postStatus = GetURLParameter('post');
if(postStatus=='success'){
    jQuery('#btnRequestSample').attr('disabled','disabled').prop('disabled',true);
    jQuery('.srpform').show();
    jQuery('.srpform__success').show();
    jQuery('.srpform__form').hide();
}
srpApp.btnAction = function() {
    jQuery('.js-request-sample').click(function(){
        if(!jQuery(this).is(':disabled')){

       var pname = jQuery(this).attr('data-pname');
       var pimg = jQuery(this).attr('data-pimg');
       var pprice = jQuery(this).attr('data-pprice');
       var pvid = jQuery(this).attr('data-pvid');
       var pid = jQuery(this).attr('data-pid');

       jQuery('#rfq_pname').val(pname);
       jQuery('#rfq_pimg').val(pimg);
       jQuery('#rfq_pprice').val(pprice);
       jQuery('#rfq_pvid').val(pvid);
       jQuery('#rfq_pid').val(pid);

       jQuery('.srpform').slideDown();
        }
    });
};
srpApp.productStatus = function(cid,pid) {
    var prodStatusUrl = srpApp.AppURL +"/getrequestedsamples";
    $.ajax({
        url: prodStatusUrl,
        data: 'cid='+cid+'&pid='+pid,
        jsonp: 'callback',
        dataType: "jsonp",
        success: function (data) {
            // alert('productStatus success');
            //alert('data=' + data);
            if(data.length>0){
                if(data[0].status=="REQUESTED"){
                    jQuery('.js-request-sample').addClass('disabled').text('Requested for Sample').prop('disabled',true).attr('disabled','disabled');
                }
            }else{
                jQuery('.js-request-sample').prop('disabled',false).removeAttr('disabled');
                srpApp.btnAction();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('error ' + textStatus + " " + errorThrown);
        }
    });
};
srpApp.btnAction();

$(document).on(`page:load page:change`, function() {
	console.log('inside page load & change');
});