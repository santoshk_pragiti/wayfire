var $$ = jQuery.noConflict();
var sectionContactInfo = '.section--contact-information';
var sectionShipAddress = '.section--shipping-address';
var sectionShipMethods = '.section--shipping-method';
var sectionPayMethods = '.section--payment-method';

var step_iconHtml = '<span class="step_icon"><svg height="16px" viewBox="0 -46 417.81333 417" width="16px" xmlns="http://www.w3.org/2000/svg"><path d="m159.988281 318.582031c-3.988281 4.011719-9.429687 6.25-15.082031 6.25s-11.09375-2.238281-15.082031-6.25l-120.449219-120.46875c-12.5-12.5-12.5-32.769531 0-45.246093l15.082031-15.085938c12.503907-12.5 32.75-12.5 45.25 0l75.199219 75.203125 203.199219-203.203125c12.503906-12.5 32.769531-12.5 45.25 0l15.082031 15.085938c12.5 12.5 12.5 32.765624 0 45.246093zm0 0" fill="#a46f5a"/></svg></span>';

var spcLabels = {
  contactStep: "Contact information",
  deliveryStep: "Shipping address",
  shippingStep: "Shipping method",
  paymentStep: "Payment",
  billingStep: "Billing address",
};
var shippingField = [
  "checkout_email_or_phone",
  "checkout_shipping_address_first_name",
  "checkout_shipping_address_last_name",
  "checkout_shipping_address_address1",
  "checkout_shipping_address_city",
  "checkout_shipping_address_country",
  "checkout_shipping_address_province",
  "checkout_shipping_address_zip",
  "checkout_shipping_address_phone",
];

var shippingMethodHTML =
  '<div class="section section--shipping-method ssv0"><div class="section__header"><h2 class="section__title">' +
  opcLang.shippingMethod +
  '</h2></div><div class="section__content"><fieldset class="content-box" data-shipping-methods=""><div class="content-box__row prev_step_wait">' +
  opcLang.shippingNotice +
  "</div></fieldset></div></div>",
  paymentHTML =
  '<div class="section section--payment-method ssv0"><div class="section__header"><h2 class="section__title">' +
  opcLang.payment +
  '</h2></div><div class="section__content"><fieldset class="content-box"><div class="content-box__row ship_step_wait">' +
  opcLang.paymentNotice +
  "</div></fieldset></div></div>";

var isNotShippingSubmitted = true;


function controlOrderSummaryField() {
  $$("body").click(function (e) {
    if (($$(e.target).hasClass('field__input') || $$(e.target).hasClass('field__input-wrapper')) && ($$(e.target).closest('.main__content').length)) {
      // alert("Inside main__content");
      if ($$(e.target).hasClass('field__input')) {
        $$(e.target).addClass('focusin');
      }
      if ($$(e.target).hasClass('field__input-wrapper')) {
        $$(e.target).find('.field__input').addClass('focusin');
      }
    } else {
      // alert("Outside main__content");
      $$(".main__content .field__input").removeClass('focusin');
    }
  });
  $$(".main__content .field__input")
    .not(".visually-hidden")
    .focusin(function () {
      $$(".main__content .field__input").removeClass('focusin');
      $$(this).addClass('focusin');
      disabledOrderSummary(true);
    })
    .focusout(function () {
      $$(this).removeClass('focusin');
      setTimeout(function () {
        if ($$(".main__content .focusin").length == 0) {
          disabledOrderSummary(false);
        }
      }, 6000);
    }).blur(function () {
      $$(this).removeClass('focusin');
      setTimeout(function () {
        if ($$(".main__content .focusin").length == 0) {
          disabledOrderSummary(false);
        }
      }, 6000);
    });
}

function disabledOrderSummary(state) {
  if (state) {
    $$(".order-summary__section .field__input,.order-summary__section .field__input-btn").prop("readonly", true).attr("readonly", "readonly");
  } else {
    $$(".order-summary__section .field__input,.order-summary__section .field__input-btn").prop("readonly", false).removeAttr("readonly");
  }
}

function shippingAddFieldChange() {
  $$(".section--shipping-address .field__input")
    .not(".visually-hidden")
    .focusout(function () {
      var text_val = $$(this).val();
      if (text_val !== "") {
        // console.log("text_val " + text_val);
        checkStepOne();
      }
    }).blur(function () {
      var text_val = $$(this).val();
      if (text_val !== "") {
        // console.log("text_val " + text_val);
        checkStepOne();
      }
    }); //trigger the focusout event manually
  $$(document).keypress(function (event) {
    var keycode = event.keyCode || event.which;
    if (keycode == '13') {
      checkStepOne();
    }
  });
}

function checkEmailPhoneField() {
  $$(document).on('change focusout', '#checkout_email_or_phone', function () {
    setTimeout(function () {
      if($$('#checkout_email_or_phone').attr('autocomplete')=='shipping tel'){
        $$('#checkout_shipping_address_phone').val($$("#checkout_email_or_phone").val()).attr('value',$$("#checkout_email_or_phone").val());
        $$('#checkout_shipping_address_phone').addClass('ignore');
      }
    }, 500);
  });
}

function U() {
  var z = 0;
  $$("#cartLoader").remove();
  $$(".main").css("min-height", "auto");
}

function I() {
  var p = $$(document).height();
  $$(".main").css("min-height", p);
  if ($$("#cartLoader").length > 0) {
    $$("#cartLoader").css("min-height", $$(document).height());
  } else {
    $$("body").append(
      '<div id="cartLoader" style="left:0;top:0;height:' +
      p +
      'px;width:100%;text-align:center;background-color: rgba(255, 255, 255, .5);position:absolute;z-index:9999999999"><img style="position:fixed;top:70px;left:50%;margin-left:-100px;" src="https://d17awlyy7mou9o.cloudfront.net/shopify/images/opc_loader.gif" /></div>'
    );
  }
}

I();

function checkStepOne() {
  $$('#checkout_shipping_address_zip,#checkout_shipping_address_id,#checkout_email_or_phone,#checkout_shipping_address_phone').change(function () {
    isNotShippingSubmitted = true;
  });
  $$('#checkout_shipping_address_zip,#checkout_shipping_address_id,#checkout_email_or_phone,#checkout_shipping_address_phone').click(function () {
    isNotShippingSubmitted = true;
  });
  if ($$(".contact_informationform").length) {
    var checkShippingFormState = checkShippingForm();
    var formValidation = $$(".contact_informationform").valid();
    console.log('inside checkStepOne formValidation ' + formValidation);
    console.log('checkShippingFormState ' + checkShippingFormState);
    console.log('isNotShippingSubmitted ' + isNotShippingSubmitted);
    if (formValidation && checkShippingFormState && isNotShippingSubmitted) {
      console.log('inside submitFirstStep');
      $$('.section--contact-information .section__title .step_icon').remove();
      $$('.section--shipping-address .section__title .step_icon').remove();
      $$('.section--contact-information .section__title,.section--shipping-address .section__title').append(step_iconHtml);
      updateHtmlFromVal(".section--contact-information .field__input");
      updateHtmlFromVal(".section--shipping-address .field__input");
      submitFirstStep();
      isNotShippingSubmitted = false;
    }
  }
}

function initFormValidation() {
  if ($$(".main__content .edit_checkout").length) {
    $$('.main__content .edit_checkout .field__input[aria-required="true"]').not(".visually-hidden").addClass("required");
    $$(".main__content .edit_checkout").validate({
      ignore: ".ignore"
    });
  }
}

function paymentPage() {
  $$(".review-block").parents(".section").hide();
}

function S() {
  return 1300 > $$(window).width() ? !0 : !1;
}

function L() {
  $$(".step__footer").css("display", "block");
  $$(".step__footer button").css("width", "100%");
  $$(".step__footer button span").text(opcLang.completeOrder);
  $$(".step__footer .shown-if-js").css("width", "100%");
  if (
    1300 > $$(window).width() &&
    opcSettings.boosters &&
    1 == opcSettings.boosters.booster5
  ) {
    $$(".step__footer").css("position", "fixed");
    $$(".step__footer").css("overflow", "hidden");
    $$(".step__footer").css("bottom", "0");
    $$(".step__footer").css("left", "0");
    $$(".step__footer").css("width", "100%");
    $$(".step__footer__previous-link").hide();
    var a = 0;
    $$(document).scroll(function () {
      setTimeout(function () {
        a = $$("#guaranteeImgBtn").length ?
          $$(document).height() - 140 - $$("#guaranteeImgBtn").outerHeight(!0) :
          $$(document).height() - 140;
        $$(window).scrollTop() + $$(window).height() > a ||
          !$$(".ship_step_wait").length ?
          ($$(".step__footer").css("position", "relative"),
            $$(".step__footer__previous-link").show(),
            $$("#stepFooterHolder").remove()) :
          ($$("#stepFooterHolder").length ||
            ($$(".step__footer").after('<div id="stepFooterHolder"></div>'),
              $$("#stepFooterHolder").css("width", "100%"),
              $$("#stepFooterHolder").css(
                "height",
                $$(".step__footer").outerHeight(!0) + "px"
              )),
            $$(".step__footer").css("position", "fixed"),
            $$(".step__footer").css("bottom", "0"),
            $$(".step__footer").css("left", "0"),
            $$(".step__footer").css("width", "100%"),
            $$(".step__footer__previous-link").hide());
      }, 500);
    });
  }
}

function setSectionHeadings() {
  if ($$(".section--contact-information .section__title").length) {
    $$(".section--contact-information .section__title").text(
      "1. " + spcLabels.contactStep
    );
  }
  if ($$(".section--shipping-address .section__title").length) {
    $$(".section--shipping-address .section__title").text(
      "2. " + spcLabels.deliveryStep
    );
  }
  if ($$(".section--shipping-method .section__title").length) {
    $$(".section--shipping-method .section__title").text(
      "3. " + spcLabels.shippingStep
    );
  }
  if ($$(".section--payment-method .section__title").length) {
    $$(".section--payment-method .section__title").text(
      "4. " + spcLabels.paymentStep
    );
  }
  if ($$(".section--billing-address .section__title").length) {
    $$(".section--billing-address .section__title").text(
      "5. " + spcLabels.billingStep
    );
  }
  if (Shopify.Checkout.step != "payment_method") {
    $$('.step__footer .btn').addClass('disabled').prop("disabled", true);
  }
  $$('.step__footer__previous-link-content').text('Return to cart');
  $$('.step__footer__previous-link').attr('href', 'https://' + Shopify.Checkout.apiHost + '/cart');

  $$('.section__title .step_icon').remove();
  if (Shopify.Checkout.step == "shipping_method") {
    $$('.section--contact-information .section__title,.section--shipping-address .section__title').append(step_iconHtml);
  } else if (Shopify.Checkout.step == "payment_method") {
    $$('.section--shipping-method .section__title,.section--shipping-address .section__title,.section--contact-information .section__title').append(step_iconHtml);
  } else {
    console.log('processing...');
  }
}

function modifyLayout2(stepName) {
  // $$('#discount-info').css('padding','0');
  $$(".siderbar").addClass("threeCol"),
    $$(".content").addClass("threeCol"),
    $$(".section--remember-me").remove();

  if ($$("#oldSteps").length === 0) {
    $$(".step").first().before('<div id="oldSteps"></div>');
  }
  $$("#oldSteps").wrap(
    "<div class='leftCol'></div>"
  );
  if (stepName == "shipping_method") {
    $$('div[data-step="shipping_method"]').first().appendTo(".leftCol");
  }
  if ($$(".rightCol").length === 0 && stepName == "payment_method") {
    $$('div[data-step="payment_method"]').wrap(
      "<div class='rightCol'></div>"
    );
    $$('.rightCol .section').first().appendTo(".leftCol");
  } else {
    $$('.leftCol').after("<div class='rightCol'></div>");
    $$(".rightCol").append(paymentHTML);
  }
  // $$('div[data-step="contact_information"]').first().appendTo(".leftCol");
  // $$('div[data-step="shipping_method"]').first().appendTo(".leftCol");
  // $$(".section--billing-address").hide();
  // $$(".section--billing-address").first().appendTo(".rightCol");
  // $$(".step__footer").first().appendTo(".rightCol");
  // if($$('.leftCol .section').length === 0) {
  //  $$('.rightCol .section').first().appendTo(".leftCol");
  // }
  $$(".section--payment-method").css({
    margin: "0",
    padding: "0"
  });
  $$(".main__content").css("position", "relative");
  // $$(".main").css("padding", "5px 36px 0 0");
  $$(".main__header").css("padding-bottom", "0");
  $$("input[name='checkout[remember_me]']").parents(".field").hide();
  $$(".section__header").css("margin-bottom", "5px");
  $$(".section:not(.section .section)").css("padding-top", "30px");
  $$(".section--contact-information").css("padding-top", "0");
  $$(".section--contact-information .section__header").css(
    "margin-top",
    "30px"
  );
  $$(".section--contact-information").css("padding-top", "0");
  // $$(".sidebar").css("width", "288px");
  // $$(".wrap").css("max-width", "1200px");
  setSectionHeadings();
  setInterval(function () {
    $$('.rightCol .review-block').closest('.section').addClass('hidden');
    setSectionHeadings();
    if ($$(".main__content .focusin").length == 0) {
      disabledOrderSummary(false);
    }
  }, 500);
}

function modifyLayout(stepName) {
  $$('#discount-info').css('padding', '0');
  $$(".field:not(.field--email_or_phone,.field--third)").each(function () {
    $$(this).find("#checkout_reduction_code").length ||
      $$(this).find("#checkout_email").length ||
      $$(this).addClass("field--half");
  });
  setInterval(function () {
    // $$(".field:not(.field--email_or_phone,.field--third)").each(function () {
    //   $$(this).hasClass("field--half") ||
    //     $$(this).find("#checkout_reduction_code").length ||
    //     $$(this).find("#checkout_email").length ||
    //     $$(this).addClass("field--half");
    // });
    updateHtmlFromVal(".section--contact-information .field__input");
    updateHtmlFromVal(".section--shipping-address .field__input");
    $$('.rightCol .review-block').closest('.section').addClass('hidden');
    // $$(".step").css("min-height", $$(".rightCol").height() + "px");
  }, 500);
  $$(".siderbar").addClass("threeCol"),
    $$(".content").addClass("threeCol"),
    $$(".section--remember-me").remove(),
    // $$(".section--shipping-method").addClass("field--half"),
    // $$(".section--payment-method").addClass("field--half");

    $$(".step[data-step='contact_information']").wrap(
      "<div class='leftCol'></div>"
    );
  $$(".section--shipping-method").first().appendTo(".leftCol");
  // $$(".leftCol").append(shippingMethodHTML);
  if ($$(".rightCol").length === 0 && $$(".section--payment-method").length > 0) {
    $$(".section--payment-method").wrap(
      "<div class='rightCol'></div>"
    );
  } else {
    $$('.leftCol').after("<div class='rightCol'></div>");
    $$(".rightCol").append(paymentHTML);
  }
  // $$(".section--billing-address").hide();
  // $$(".section--billing-address").first().appendTo(".rightCol");
  $$(".step__footer").first().appendTo(".rightCol");

  $$(".section--payment-method").css({
    margin: "0",
    padding: "0"
  });
  $$(".main__content").css("position", "relative");
  // $$(".main").css("padding", "5px 36px 0 0");
  $$(".main__header").css("padding-bottom", "0");
  $$("input[name='checkout[remember_me]']").parents(".field").hide();
  $$(".section__header").css("margin-bottom", "5px");
  $$(".section:not(.section .section)").css("padding-top", "30px");
  $$(".section--contact-information").css("padding-top", "0");
  $$(".section--contact-information .section__header").css(
    "margin-top",
    "30px"
  );
  $$(".section--contact-information").css("padding-top", "0");
  // $$(".sidebar").css("width", "288px");
  // $$(".wrap").css("max-width", "1200px");
  setSectionHeadings();
}

function updateHtmlFromVal(selector) {
  $$(selector).each(function () {
    if (!$$(this).hasClass('field__input--select')) {
      $$(this).attr('value', $$(this).val());
      $$(this).attr('data-value', $$(this).val());
    }
  });
}

function fetchPayment() {
  $$(
    ".section--shipping-method .section__content .content-box[data-shipping-methods]"
  ).removeClass("opcErrorBox");
  var a, b, e, c, d;
  $$("#checkout_email_or_phone").removeAttr("data-autofocus");
  $$("#checkout_email").removeAttr("data-autofocus");
  // if (q) {
  //   var g = $$(".step .edit_checkout").first();
  //   $$('input[name="checkout[email_or_phone]"]').val(
  //     $$("#checkout_email_or_phone").val()
  //   );
  // } else
  var g = $$(".edit_checkout[data-shipping-method-form]");
  var h = g.attr("action");
  var x = !1;
  I();
  disabledOrderSummary(true);
  $$.ajax({
      type: "POST",
      url: h,
      data: g.serialize()
    })
    .done(function (f) {
      if (f == "" || $$(".section--shipping-method .notice--error", f).length) {
        window.location.href = 'https://' + Shopify.Checkout.apiHost + "checkout?step=shipping_method&spc=false";
        U();
      } else {
        $$("head").find("script").remove();
        var m = f.split("\x3c!--<![endif]--\x3e")[1].split("</head>")[0];
        c = m.match(
          /<script src="\/\/cdn.shopify.com\/s\/assets\/checkout(.*)><\/script>/
        );
        m = m.replace(c[0], "");
        m = m.replace(
          "&quot;client_attributes_checkout&quot;;",
          "'client_attributes_checkout';"
        );
        $$("head").append(m);
        setTimeout(function () {
          $$("head").append(c[0]);
        }, 1700);
        b = $$(window).scrollTop();
        f = f.substring(f.indexOf("<body>") + 6, f.indexOf("</body>"));
        // $$("form.edit_checkout .section--payment-method").remove();
        $$(".main__content form.edit_checkout input").attr("value", function () {
          return $$(this).val();
        });
        $$(".main__content form.edit_checkout select option:checked").attr("checked2", "1");
        $$(".main__content form.edit_checkout select option").removeAttr("selected");
        $$(".main__content form.edit_checkout select option[checked2='1']")
          .attr("selected", "selected")
          .removeAttr("checked2");
        $$('.main__content form.edit_checkout input[type="checkbox"]:checked').attr(
          "checked2",
          "1"
        );
        $$('.main__content form.edit_checkout input[type="checkbox"]').removeAttr("checked");
        $$(".main__content form.edit_checkout input[checked2='1']")
          .attr("checked", "checked")
          .removeAttr("checked2");
        $$('.section--shipping-method input[type="radio"]').removeAttr(
          "checked"
        );
        $$(".section--shipping-method input[value='" + k + "']").attr(
          "checked",
          "checked"
        );
        $$(".step__footer").remove();
        $$(".step__footer a").remove();
        $$(".rightCol").remove();
        // debugger;
        if ($$('div[data-step="contact_information"]').length > 0) {
          var oldContactInfoForm = $$('<div>').append($$('div[data-step="contact_information"]').clone()).html();
        }
        var shippingMethodForm = '';
        if ($$('div[data-step="shipping_method"]').length > 0) {
          var shippingMethodForm = $$('<div>').append($$('div[data-step="shipping_method"]').clone()).html();
        } else if ($$('div.section--shipping-method').length > 0) {
          var shippingMethodForm = $$('<div>').append($$('.section--shipping-method').first().clone()).html();
        } else {
          console.error('No Shipping method available');
          var forceUrl = 'https://' + Shopify.Checkout.apiHost + "checkout?step=shipping_method&spc=false";
          window.location.href = forceUrl;
        }
        // var oldEditCheckoutForm = $$("form.edit_checkout")[0].outerHTML;
        // oldEditCheckoutForm += $$(".section--shipping-method")[0].outerHTML;
        oldSidebar = $$(".sidebar").html();
        oldMainheader = $$(".main__header").html();
        oldFooterA = $$(".step__footer a");
        $$("body").empty();
        $$("body").append('<div id="opcWrapper">' + f + "</div>");
        $$(".content").addClass("mb_opc");
        $$('#opcWrapper .review-block').closest('.section').addClass('hidden');
        $$('div[data-step="payment_method"]').find('.edit_checkout').prev().hide();
        // $$(".sidebar").html(oldSidebar);
        // $$(".main__header").html(oldMainheader);
        // $$(".step__footer").append(oldFooterA);
        $$("body .step").first().before('<div id="oldSteps"></div>');

        if (oldContactInfoForm) {
          $$("#oldSteps").append(oldContactInfoForm);
        }
        if (shippingMethodForm) {
          $$("#oldSteps").append(shippingMethodForm);
        }
        // $$("#section--billing-address__different").hide();
        // $$(".review-block").parents(".section").hide();
        $$('.section').removeClass('checkout--active').addClass('checkout--inactive');
        $$('.section--payment-method,.section--billing-address').removeClass('checkout--inactive');

        // L();
        window.scrollTo(0, b);
        // modifyLayout();
        // $$(".section--payment-method .content-box__row--secondary").css({
        //   display: "block",
        //   overflow: "hidden",
        //   padding: "0px",
        //   height: "0px",
        // });
        $$('.step__footer__previous-link').hide();
        if ($$(".main__content .focusin").length == 0) {
          disabledOrderSummary(false);
        }
        U();
      }
    })
    .fail(function () {
      U();
      if ($$(".main__content .focusin").length == 0) {
        disabledOrderSummary(false);
      }
      setTimeout(function () {
        console.log("fail submit second step");
      }, 4e3);
    });
}

function initShippingSelection() {
  $$("body").on(
    "change",
    ".section--shipping-method input[type=radio]",
    function () {
      k = this.value;
      $$("div[data-shipping-warning]").remove();
      console.log("inside initShippingSelection");
      updateHtmlFromVal(".section--contact-information .field__input");
      updateHtmlFromVal(".section--shipping-address .field__input");
      $$(".section--shipping-method input[type=radio]").removeAttr("checked");
      $$(this).prop("checked", true).attr("checked", "checked");
      fetchPayment();
      // jQuery('#continue_button').trigger('click');
    }
  );
}
window.submitFirstStep = function (a) {
  var firstStepTry = a;
  console.log("inside submitFirstStep");
  var c = $$(".main__content form.edit_checkout").first();
  var h = c.attr("action");
  if (firstStepTry == "2") {
    h = h + '?step=shipping_method';
  }
  I();
  // shipping method selected
  // debugger;
  disabledOrderSummary(true);
  $$.ajax({
      type: "post",
      url: h,
      data: c.serialize(),
      success: function (f) {
        // alert('success');
        if (f == "") {
          var forceUrl = 'https://' + Shopify.Checkout.apiHost + "checkout?step=contact_information&spc=false";
          window.location.href = forceUrl;
        } else {
          console.log('success ');
          // debugger;
          console.log('shipping method selected response' + f);
          f = $$(".step .edit_checkout", f);
          shippingMethod = $$(".section--shipping-method", f);
          var shippingError = $$("div.field--error", f);
          console.log('shippingError ' + shippingError.length);
          if (shippingError.length > 0) {
            $$('.section__title .step_icon').remove();
            // debugger;
            // var shippingAddress = $$(".section--shipping-address", f);
            // $$(".section--shipping-address").html(shippingAddress);
            shippingError.each(function () {
              var fieldId = $$(this).find('.field__input').attr('id');
              var emailPhoneField = $$(this).attr('data-email-or-phone-input-wrapper');
              var fieldName = $$(this).attr('data-address-field');
              var fieldError = $$(this).find('.field__message--error').html();
              $$('#' + fieldId).closest('.field').find('.field__message').remove();
              if (emailPhoneField) {
                $$('#' + fieldId).closest('.field').addClass('field--error').append('<p class="field__message field__message--error" id="error-for-' + fieldName + '">' + fieldError + '</p>');
              } else {
                $$('#' + fieldId).closest('.field').addClass('field--error').append('<p class="field__message field__message--error" id="error-for-checkout_email_or_phone">' + fieldError + '</p>');
              }
            });
            $$(".field--error:visible").first().find('.field__input').focus();
            if ($$('#shipping').length > 0) {
              $$('#shipping').empty().append('<fieldset class="content-box" data-shipping-methods=""><div class="content-box__row prev_step_wait">' + opcLang.shippingNotice + '</div></fieldset>');
            } else {
              $$('.section--shipping-method .section__content').empty().append('<fieldset class="content-box" data-shipping-methods=""><div class="content-box__row prev_step_wait">' + opcLang.shippingNotice + '</div></fieldset>');
            }
            $$('.section--shipping-method .section__text').remove();
            $$('.section--payment-method .section__content').empty().append('<fieldset class="content-box"><div class="content-box__row ship_step_wait">' + opcLang.paymentNotice + '</div></fieldset>');
            shippingAddFieldChange();
            console.log('shippingadd error ' + shippingError);
          } else {
            // debugger;
            console.log('shippingadd No error ' + shippingError);
            console.log('shippingMethod ' + shippingMethod);
            var m = null;
            f.find(".section__header p").length && (m = f.find(".section__header p"));
            f.find(".section__header").remove();
            f.find(".step__footer").remove();
            f.find(".step__sections .section").first().remove();
            $$(".section--shipping-method .section__content").remove();
            $$("#shipping").remove();
            $$(".section--shipping-method").append(
              "<div id='shipping'>" + f[0].outerHTML + "</div>"
            );
            var shippingMethodsCount = jQuery('div[data-shipping-method]').length;
            // alert('shippingMethodsCount ' + shippingMethodsCount);
            $$(".section--shipping-method .section__header p").remove();
            m &&
              $$(".section--shipping-method .section__header").append(m[0].outerHTML);
            f.find(".content-box__row--secondary").hide();
            initShippingSelection();
            if (shippingMethodsCount === 0) {
              if (!firstStepTry) {
                I();
                submitFirstStep('2');
              } else {
                var forceUrl = 'https://' + Shopify.Checkout.apiHost + "checkout?step=shipping_method&spc=false";
                window.location.href = forceUrl;
              }
            } else {
              $$('.section--shipping-method input:checked').prop("checked", false).removeAttr('checked');
              $$('.section').removeClass('checkout--active').addClass('checkout--inactive');
              $$('.section--shipping-method').removeClass('checkout--inactive');
              $$('.section--shipping-method .section__title').after('<p class="section__text">Select shipping method to proceed for payment</p>');
              U();
              // jQuery('div[data-shipping-method]').first().find('.input-radio').prop("checked",true).attr('checked',true).trigger('change');

            }
          }
          U();
        }
        if ($$(".main__content .focusin").length == 0) {
          disabledOrderSummary(false);
        }
      },
      error: function (data) {
        // alert('error');
        if ($$(".main__content .focusin").length == 0) {
          disabledOrderSummary(false);
        }
        console.log('error ' + data);
      }
    })
    .done(function (f) {})
    .fail(function () {
      U();
      console.log("fail submit first step");
    });
};

function checkShippingForm() {
  var isValid = true;
  $$('.section--shipping-address input[aria-required="true"]')
    .not(".visually-hidden").not('.ignore')
    .each(function () {
      if ($$(this).val() === "") {
        isValid = false;
        console.log("Field empty is " + $$(this).attr("id"));
        return false;
      }
    });
  return isValid;
}

function enableLeftDiscount() {
  if($$('.main__content .left-discount').length==0) {
    $$('.main__content').before('<div class="left-discount"><h3>Have Discount code?</h3><div class="order-summary__section order-summary__section--discount" data-reduction-form="update"></div></div>');
    $$('.left-discount .order-summary__section').html($$('.sidebar .order-summary__section--discount').html());
    $$('.sidebar .order-summary__section--discount').remove();
  }
}

function initSPC() {
  if (Shopify.Checkout.page === "show" && !opcSettings.pageSetupCompleted) {
    // on load update headers
    $$('body').addClass('spc');
    // modifyLayout();
    if (Shopify.Checkout.step == "contact_information") {
      $$('[data-step="contact_information"] form:first').addClass('contact_informationform');
      checkStepOne(); // check form on page load
      $$('.step__footer__continue-btn').hide();
      $$('.step__footer__previous-link').before('<button name="button" type="submit" id="continue_button" class="step__footer__continue-btn btn" aria-busy="false"><span class="btn__content" data-continue-button-content="true">Complete order</span><svg class="icon-svg icon-svg--size-18 btn__spinner icon-svg--spinner-button" aria-hidden="true" focusable="false"> <use xlink:href="#spinner-button"></use> </svg></button>');
      modifyLayout("contact_information");
      $$('.section').removeClass('checkout--active').addClass('checkout--inactive');
      $$('.section--contact-information,.section--shipping-address').addClass('checkout--active');
      $$('.section__title .step_icon').remove();
    }
    if (Shopify.Checkout.step === "shipping_method") {
      // $$(".step__footer").before(paymentHTML);
      setSectionHeadings();
      initShippingSelection();
      $$('.step__footer__continue-btn').hide();
      modifyLayout2("shipping_method");
      $$('.rightCol').append('<div class="step__footer" data-step-footer=""><button name="button" type="submit" id="continue_button" class="step__footer__continue-btn btn disabled" disabled aria-busy="false"><span class="btn__content" data-continue-button-content="true">Complete order</span><svg class="icon-svg icon-svg--size-18 btn__spinner icon-svg--spinner-button" aria-hidden="true" focusable="false"> <use xlink:href="#spinner-button"></use> </svg></button></div>');

      $$(".section--shipping-address .field__input")
        .not(".visually-hidden")
        .focusout(function () {
          var text_val = $$(this).val();
          if (text_val !== "") {
            // console.log("text_val " + text_val);
            checkStepOne();
          }
        }).blur(function () {
          var text_val = $$(this).val();
          if (text_val !== "") {
            // console.log("text_val " + text_val);
            checkStepOne();
          }
        }); //trigger the focusout event manually
      $$(document).keypress(function (event) {
        var keycode = event.keyCode || event.which;
        if (keycode == '13') {
          checkStepOne();
        }
      });

      // $$('.section').removeClass('checkout--active').addClass('checkout--inactive');
      // $$('.section--shipping-method').removeClass('checkout--inactive');
    }
    if (Shopify.Checkout.step === "payment_method") {
      setSectionHeadings();
      $$('.step__footer__continue-btn').hide();
      $$('.step__footer__previous-link').before('<button name="button" type="submit" id="continue_button" class="step__footer__continue-btn btn" aria-busy="false"><span class="btn__content" data-continue-button-content="true">Complete order</span><svg class="icon-svg icon-svg--size-18 btn__spinner icon-svg--spinner-button" aria-hidden="true" focusable="false"> <use xlink:href="#spinner-button"></use> </svg></button>');
      modifyLayout2("payment_method");
      $$('.step__footer .btn').removeClass('disabled').prop("disabled", false);
      // $$('.section').removeClass('checkout--active').addClass('checkout--inactive');
      // $$('.section--shipping-method').removeClass('checkout--inactive').addClass('checkout--active');
    }
    opcSettings.pageSetupCompleted = true;
    $$('.section').removeClass('checkout--active').addClass('checkout--inactive');
    $$('div[data-step="' + Shopify.Checkout.step + '"] .section').removeClass('checkout--inactive').addClass('checkout--active');

    $$(".section--shipping-address .field__input")
      .not(".visually-hidden")
      .focusout(function () {
        var text_val = $$(this).val();
        if (text_val !== "") {
          // console.log("text_val " + text_val);
          checkStepOne();
        }
      }).blur(function () {
        var text_val = $$(this).val();
        if (text_val !== "") {
          // console.log("text_val " + text_val);
          checkStepOne();
        }
      }); //trigger the focusout event manually
    $$(document).keypress(function (event) {
      var keycode = event.keyCode || event.which;
      if (keycode == '13') {
        checkStepOne();
      }
    });

    U();
  }
}

function init() {
  var currentStep = Shopify.Checkout.step;
  console.log('shopify page load');
  if (currentStep == "contact_information") {
    initFormValidation();
    $$('[data-step="contact_information"] form:first').addClass('contact_informationform');
    $$(".step__footer").before(shippingMethodHTML);
    shippingAddFieldChange();
  }
  setTimeout(function () {
    console.log('inside 1sec function with step ' + Shopify.Checkout.step);
    initSPC();
  }, 1001);
  U();
}

$$(document).on('page:change', function () {
  alert('page:change22');
  initFormValidation();
  $$('[data-step="contact_information"] form:first').addClass('contact_informationform');
  checkEmailPhoneField();
  controlOrderSummaryField();
  shippingAddFieldChange();
  $$('#checkout_email_or_phone').attr('type', 'text');
  $$('#checkout_shipping_address_id').addClass('ignore');
  initShippingSelection();
  setSectionHeadings();
});

$$(document).on('page:load', function () {
  console.log('OPC setting ' + opcSettings.enabled);
  // enableLeftDiscount(); // show discount
  if ($$('.email-msg2').length == 0) {
    $$('.field--email_or_phone').after('<div class="email-msg2" style="margin:10px 0"><span style="color:red">*</span> Email or mobile phone number are required to provide order and shipping information.</div>');
  }
  if ($$(".section--contact-information .logged-in-customer-information__paragraph").length > 0) {
    $$("#checkout_email_or_phone").hide();
  }
  checkEmailPhoneField();
  controlOrderSummaryField();
  $$('#checkout_email_or_phone').attr('type', 'text');
  $$('#checkout_shipping_address_id').addClass('ignore');
  init();
});


// end Shopify page load